pragma solidity 0.5.17;


/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint a, uint b) internal pure returns (uint) {
        uint c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint a, uint b) internal pure returns (uint) {
        require(b <= a, "SafeMath: subtraction overflow");
        uint c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint a, uint b) internal pure returns (uint) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint a, uint b) internal pure returns (uint) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0, "SafeMath: division by zero");
        uint c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint a, uint b) internal pure returns (uint) {
        require(b != 0, "SafeMath: modulo by zero");
        return a % b;
    }
}


interface IERC20 {
    function totalSupply() external view returns (uint);
    function balanceOf(address account) external view returns (uint);
    function transfer(address recipient, uint amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint amount) external returns (bool);
    function approve(address spender, uint amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint);
    function decimals() external view returns(uint);
    event Transfer(address indexed from, address indexed to, uint value);
    event Approval(address indexed owner, address indexed spender, uint value);
}


interface IUniswap {
    function tokenAddress() external view returns (address);
    function ethToTokenSwapInput(uint, uint) external payable returns (uint);
    function ethToTokenSwapOutput(uint, uint) external payable returns (uint);
    function tokenToEthSwapInput(uint, uint, uint) external returns (uint);
    function tokenToEthSwapOutput(uint, uint, uint) external returns (uint);
    function getEthToTokenInputPrice(uint) external view returns (uint);
    function getEthToTokenOutputPrice(uint) external view returns (uint);
    function getTokenToEthInputPrice(uint) external view returns (uint);
}


interface IUniswapFactory {
    function getExchange(address) external returns (IUniswap);
    function getToken(IUniswap) external returns (IERC20);
}


interface IKyberNetwork {
    function maxGasPrice() external view returns(uint);
    function getUserCapInWei(address user) external view returns(uint);
    function getUserCapInTokenWei(address user, address token) external view returns(uint);
    function enabled() external view returns(bool);
    function info(bytes32 id) external view returns(uint);
    function getExpectedRate(address src, address dest, uint srcQty) external view
        returns (uint expectedRate, uint slippageRate);
    function tradeWithHint(address trader, address src, uint srcAmount, address dest, address destAddress,
        uint maxDestAmount, uint minConversionRate, address walletId, bytes calldata hint) external payable returns(uint);
}


interface IKyberNetworkProxy {
    function maxGasPrice() external view returns(uint);
    function getUserCapInWei(address user) external view returns(uint);
    function getUserCapInTokenWei(address user, address token) external view returns(uint);
    function enabled() external view returns(bool);
    function info(bytes32 id) external view returns(uint);
    function getExpectedRate(address src, address dest, uint srcQty) external view
        returns (uint expectedRate, uint slippageRate);
    function tradeWithHint(address src, uint srcAmount, address dest, address destAddress, uint maxDestAmount,
        uint minConversionRate, address walletId, bytes calldata hint) external payable returns(uint);
    function trade(address src, uint srcAmount, address dest, address destAddress, uint maxDestAmount, 
        uint minConversionRate, address walletId) external payable returns (uint);
}


interface IBancor {
    function convert(address[] calldata _path, uint _amount, uint _minReturn) external payable returns (uint);
    function getReturnByPath(address[] calldata _path, uint _amount) external view returns (uint, uint);
    function claimAndConvert(address[] calldata _path, uint _amount, uint _minReturn) external returns (uint);
}


interface IGasToken {
    function free(uint value) external returns (bool success);
    function freeUpTo(uint value) external returns (uint freed);
    function freeFrom(address from, uint value) external returns (bool success);
    function freeFromUpTo(address from, uint value) external returns (uint freed);
    function balanceOf(address) external view returns (uint);
    function transfer(address recipient, uint amount) external returns (bool);
}


interface IUniswapV2Factory {
    function getPair(address tokenA, address tokenB) external view returns (address pair);
    function allPairs(uint) external view returns (address pair);
    function allPairsLength() external view returns (uint);
}


interface IUniswapV2Router2 {
    function getAmountsOut(uint amountIn, address[] calldata path) external view returns (uint[] memory);
    function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline) external payable returns (uint[] memory);
    function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory);
}


contract Trader {
    using SafeMath for uint;
    
    // General contract
    address payable private owner;
    // address payable[] private pwners;
    // address payable private trickster;
    // bool private tricking;
    // uint private constant CONTRACT_INPUT_TO_TEST_TARGETS_PRICE = 10**17;
    // mapping(uint => Trade) private trades; // too much gas to use this
    // uint private constant MAX_UINT256 = 115792089237316195423570985008687907853269984665640564039457584007913129639935;
    uint private constant BIG_UINT = 2**254;
    // uint private constant MAX_PWNER_BAL = 10**17;
    
    // Kyber
    IKyberNetworkProxy private constant kyberProxy = IKyberNetworkProxy(0x818E6FECD516Ecc3849DAf6845e3EC868087B755);
    address private constant ETH_TOKEN_ADDRESS = address(0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE);
    // uint private constant ETH_DECIMALS = 18;
    // bytes private constant PERM_HINT = "PERM";
    
    // Uniswap
    IUniswapFactory private constant uniswapFactory = IUniswapFactory(0xc0a47dFe034B400B47bDaD5FecDa2621de6c4d95);
    uint private constant DEADLINE = 2000000000;
    
    // Bancor
    IBancor private constant bancorNetwork = IBancor(0x3Ab6564d5c214bc416EE8421E05219960504eeAD);
    
    // GST
    // IGasToken private GST1 = IGasToken(0x88d60255F917e3eb94eaE199d827DAd837fac4cB);
    IGasToken private constant GST2 = IGasToken(0x0000000000b3F879cb30FE243b4Dfee438691c04);
    
    
    // UniswapV2
    IUniswapV2Router2 private constant uniV2Router2 = IUniswapV2Router2(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D);
    
    
    // struct Trade{
    //     address src;
    //     address dest;
    //     uint srcQty;
    //     uint minTestOutput;
    // }
    
    /*
     TODO: on prod launch:
     remove test events
    */
    constructor() public {
        owner = msg.sender;
        // pwners = [owner, REPLACE, REPLACE, REPLACE, REPLACE, REPLACE];
        // trickster = msg.sender;
        // tricking = false;
        // DRGN
        approveToken(IERC20(0x419c4dB4B9e25d6Db2AD9691ccb832C8D9fDA05E));
    }
    
    
    function buyToken(uint _exID, address _tokenAddr, address[] memory _bancorPath, address[] memory _uniV2Path, uint _srcExIn) private returns (uint actualSrcExOut) {
        // Uniswap
        if (_exID == 0) {
            actualSrcExOut = uniswapFactory.getExchange(_tokenAddr).ethToTokenSwapInput.value(_srcExIn)(1, DEADLINE);
        // Kyber
        } else if (_exID == 1) {
            actualSrcExOut = kyberProxy.trade.value(_srcExIn)(ETH_TOKEN_ADDRESS, _srcExIn, _tokenAddr, address(this), BIG_UINT, 1, address(this));
        // Bancor
        } else if (_exID == 2) {
            actualSrcExOut = bancorNetwork.convert.value(_srcExIn)(_bancorPath, _srcExIn, 1);
        // UniswapV2
        } else if (_exID == 3) {
            uint[] memory amountsOut = uniV2Router2.swapExactETHForTokens.value(_srcExIn)(1, _uniV2Path, address(this), DEADLINE);
            actualSrcExOut = amountsOut[amountsOut.length-1];
        }
    }
    
    function sellToken(uint _exID, address _tokenAddr, address[] memory _bancorPath, address[] memory _uniV2Path, uint _actualSrcExOut) private {
        // Uniswap
        if (_exID == 0) {
            uniswapFactory.getExchange(_tokenAddr).tokenToEthSwapInput(_actualSrcExOut, 1, DEADLINE);
        // Kyber
        } else if (_exID == 1) {
            kyberProxy.trade(_tokenAddr, _actualSrcExOut, ETH_TOKEN_ADDRESS, address(this), BIG_UINT, 1, address(this));
        // Bancor
        } else if (_exID == 2) {
            bancorNetwork.claimAndConvert(_bancorPath, _actualSrcExOut, 1);
        // UniswapV2
        } else if (_exID == 3) {
            uniV2Router2.swapExactTokensForETH(_actualSrcExOut, 1, _uniV2Path, address(this), DEADLINE);
        }
    }
    
    function GSTFreeUpTo(uint256 _numTokens) private {
        if (_numTokens != 0) {
            uint256 safe_num_tokens = 0;
    		uint256 gas = gasleft();
    
    		if (gas >= 27710) {
                // No need to use math.sub here, extra gas
    			safe_num_tokens = (gas - 27710) / (7020);
    		}
    
    		if (_numTokens > safe_num_tokens) {
    			_numTokens = safe_num_tokens;
    		}
    		
            GST2.freeUpTo(_numTokens); 
        }
    }
    

    /*
     |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     Non-descript names are used so that competitors can't easily figure
     out what a function does by using a rainbow table-like system.
     Random letters are appended to the most used function names such that
     the methodID number equivalent is small, so it's first in the list of
     methods, therefore saving gas
     |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    */
    

    /*
     targetBoughtToken == true
    */
    function oneX(
        address _targetAddr,
        uint _targetEthAmount,
        uint _numGstsMiss,
        uint _numGstsHit,
        uint _targetExID, 
        uint _otherExID, 
        uint _srcExOut, 
        uint _destExTestOut, 
        address _tokenAddr,
        address[] calldata _bancorPath,
        address[] calldata _uniV2Path,
        uint _srcExIn
    ) external returns (bool) {
        
        // Cheap test to see if the target's transaction has been executed yet,
        // cheaper than requesting the price from an exchange
        if (_targetAddr.balance != _targetEthAmount) {
            GSTFreeUpTo(_numGstsMiss);
            return false;
        }
        
        // Test the price to see if other arbers beat us to it
        // Uniswap
        if (_targetExID == 0) {
            if (address(uniswapFactory.getExchange(_tokenAddr)).balance < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Kyber
        } else if (_targetExID == 1) {
            (uint actualDestExTestOut,) = kyberProxy.getExpectedRate(_tokenAddr, ETH_TOKEN_ADDRESS, _srcExOut);
            if (actualDestExTestOut < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Bancor
        } else if (_targetExID == 2) {
            (uint actualDestExTestOut,) = bancorNetwork.getReturnByPath(_bancorPath, _srcExOut);
            if (actualDestExTestOut < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // UniswapV2
        } else if (_targetExID == 3) {
            if (uniV2Router2.getAmountsOut(_srcExOut, _uniV2Path)[_uniV2Path.length-1] < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        }

        // To maximise gas savings, only instantiate variables after all the
        // opportunities for the tx to fail

                
        uint ethBalanceAtStart = address(this).balance;
        // Do the trade
        // Trade Eth for the token
        uint actualSrcExOut = buyToken(_otherExID, _tokenAddr, _bancorPath, _uniV2Path, _srcExIn);
        // Trade the token for Eth
        sellToken(_targetExID, _tokenAddr, _bancorPath, _uniV2Path, actualSrcExOut);
        
        require(address(this).balance > ethBalanceAtStart, "Ethx");
        GSTFreeUpTo(_numGstsHit);
        
        return true;
    }
    
        
    /*
     targetBoughtToken == false
    */
    function twoO(
        address _targetAddr,
        uint _targetEthAmount,
        uint _numGstsMiss,
        uint _numGstsHit,
        uint _targetExID, 
        uint _otherExID, 
        uint _srcExTestOut, 
        address _tokenAddr,
        address[] calldata _bancorPath,
        address[] calldata _uniV2Path,
        uint _srcExIn
    ) external returns (bool) {
        
        // Cheap test to see if the target's transaction has been executed yet,
        // cheaper than requesting the price from an exchange
        if (_targetAddr.balance != _targetEthAmount) {
            GSTFreeUpTo(_numGstsMiss);
            return false;
        }
        
        // Test the price to see if other arbers beat us to it
        // Uniswap
        if (_targetExID == 0) {
            if (address(uniswapFactory.getExchange(_tokenAddr)).balance > _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Kyber
        } else if (_targetExID == 1) {
            (uint actualSrcExTestOut,) = kyberProxy.getExpectedRate(ETH_TOKEN_ADDRESS, _tokenAddr, _srcExIn);
            if (actualSrcExTestOut < _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Bancor
        } else if (_targetExID == 2) {
            (uint actualSrcExTestOut,) = bancorNetwork.getReturnByPath(_bancorPath, _srcExIn);
            if (actualSrcExTestOut < _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // UniswapV2
        } else if (_targetExID == 3) {
            if (uniV2Router2.getAmountsOut(_srcExIn, _uniV2Path)[_uniV2Path.length-1] < _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        }

        // To maximise gas savings, only instantiate variables after all the
        // opportunities for the tx to fail
        
        
        uint ethBalanceAtStart = address(this).balance;
        // Do the trade
        // Trade Eth for the token
        uint actualSrcExOut = buyToken(_targetExID, _tokenAddr, _bancorPath, _uniV2Path, _srcExIn);
        // Trade the token for Eth
        sellToken(_otherExID, _tokenAddr, _bancorPath, _uniV2Path, actualSrcExOut);
        
        require(address(this).balance > ethBalanceAtStart, "Ethx");
        GSTFreeUpTo(_numGstsHit);
        
        return true;
    }

    /*
     |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     one_Test and two_Test are used off-chain to estimate gas use and
     simulate transactions (to determine profit amount) without the
     hassle of the transaction reverting if the arb opportunity turns
     out to not be profitable (require(address(this).balance > ethBalanceAtStart)...)
     It would be nice to use a common function that they both use, 
     but that would require declaring `ethBalanceAtStart` in 2
     functions (since it would not be visible in OneX otherwise), 
     which uses more gas
     |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    */
    
    // targetBoughtToken == true
    function oneXTest(
        address _targetAddr,
        uint _targetEthAmount,
        uint _numGstsMiss,
        uint _numGstsHit,
        uint _targetExID, 
        uint _otherExID, 
        uint _srcExOut, 
        uint _destExTestOut, 
        address _tokenAddr,
        address[] calldata _bancorPath,
        address[] calldata _uniV2Path,
        uint _srcExIn
    ) external onlyOwnerPreTx returns (bool) {
        
        // Cheap test to see if the target's transaction has been executed yet,
        // cheaper than requesting the price from an exchange
        if (_targetAddr.balance != _targetEthAmount) {
            GSTFreeUpTo(_numGstsMiss);
            return false;
        }
        
        // Test the price to see if other arbers beat us to it
        // Uniswap
        if (_targetExID == 0) {
            if (address(uniswapFactory.getExchange(_tokenAddr)).balance < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Kyber
        } else if (_targetExID == 1) {
            (uint actualDestExTestOut,) = kyberProxy.getExpectedRate(_tokenAddr, ETH_TOKEN_ADDRESS, _srcExOut);
            if (actualDestExTestOut < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Bancor
        } else if (_targetExID == 2) {
            (uint actualDestExTestOut,) = bancorNetwork.getReturnByPath(_bancorPath, _srcExOut);
            if (actualDestExTestOut < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // UniswapV2
        } else if (_targetExID == 3) {
            if (uniV2Router2.getAmountsOut(_srcExOut, _uniV2Path)[_uniV2Path.length-1] < _destExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        }

        // To maximise gas savings, only instantiate variables after all the
        // opportunities for the tx to fail
        
                
        uint ethBalanceAtStart = address(this).balance;
        // Do the trade
        // Trade Eth for the token
        uint actualSrcExOut = buyToken(_otherExID, _tokenAddr, _bancorPath, _uniV2Path, _srcExIn);
        // Trade the token for Eth
        sellToken(_targetExID, _tokenAddr, _bancorPath, _uniV2Path, actualSrcExOut);
        
        GSTFreeUpTo(_numGstsHit);
        
        return true;
    }
    
        
    // targetBoughtToken == false
    function twoOTest(
        address _targetAddr,
        uint _targetEthAmount,
        uint _numGstsMiss,
        uint _numGstsHit,
        uint _targetExID, 
        uint _otherExID, 
        uint _srcExTestOut, 
        address _tokenAddr,
        address[] calldata _bancorPath,
        address[] calldata _uniV2Path,
        uint _srcExIn
    ) external onlyOwnerPreTx returns (bool) {
        
        // Cheap test to see if the target's transaction has been executed yet,
        // cheaper than requesting the price from an exchange
        if (_targetAddr.balance != _targetEthAmount) {
            GSTFreeUpTo(_numGstsMiss);
            return false;
        }
        
        // Test the price to see if other arbers beat us to it
        // Uniswap
        if (_targetExID == 0) {
            if (address(uniswapFactory.getExchange(_tokenAddr)).balance > _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Kyber
        } else if (_targetExID == 1) {
            (uint actualSrcExTestOut,) = kyberProxy.getExpectedRate(ETH_TOKEN_ADDRESS, _tokenAddr, _srcExIn);
            if (actualSrcExTestOut < _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // Bancor
        } else if (_targetExID == 2) {
            (uint actualSrcExTestOut,) = bancorNetwork.getReturnByPath(_bancorPath, _srcExIn);
            if (actualSrcExTestOut < _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        // UniswapV2
        } else if (_targetExID == 3) {
            if (uniV2Router2.getAmountsOut(_srcExIn, _uniV2Path)[_uniV2Path.length-1] < _srcExTestOut) {
                GSTFreeUpTo(_numGstsMiss);
                return false;
            }
        }

        // To maximise gas savings, only instantiate variables after all the
        // opportunities for the tx to fail
        
        
        uint ethBalanceAtStart = address(this).balance;
        // Do the trade
        // Trade Eth for the token
        uint actualSrcExOut = buyToken(_targetExID, _tokenAddr, _bancorPath, _uniV2Path, _srcExIn);
        // Trade the token for Eth
        sellToken(_otherExID, _tokenAddr, _bancorPath, _uniV2Path, actualSrcExOut);
        
        GSTFreeUpTo(_numGstsHit);
        
        return true;
    }
    

    
    function approveForExchanges(IERC20[] memory _tokens, address[] memory _addressesToApprove, uint _amountToApprove) public onlyOwnerPreTx {
        for (uint i=0; i < _tokens.length; i++) {
            _tokens[i].approve(_addressesToApprove[i], _amountToApprove);
        }
    }
    
    function approveTokenForSingleExchange(IERC20 _token, address _exchangeToApprove) public onlyOwnerPreTx {
        _token.approve(_exchangeToApprove, BIG_UINT);
    }
    
    // function approveTokenForExchange(IERC20 _token, address _exchangeToApprove) public onlyOwnerPreTx {
    //     _token.approve(_exchangeToApprove, BIG_UINT);
    //     _token.approve(address(kyberProxy), BIG_UINT);
    //     _token.approve(address(bancorNetwork), BIG_UINT);
    // }
    
    // function approveTokenForExchangeAmount(IERC20 _token, address _exchangeToApprove, uint _amountToApprove) public onlyOwnerPreTx {
    //     _token.approve(_exchangeToApprove, _amountToApprove);
    //     _token.approve(address(kyberProxy), _amountToApprove);
    //     _token.approve(address(bancorNetwork), _amountToApprove);
    // }
    
    function approveToken(IERC20 _token) public onlyOwnerPreTx {
        _token.approve(address(kyberProxy), BIG_UINT);
        _token.approve(address(bancorNetwork), BIG_UINT);
        _token.approve(address(uniV2Router2), BIG_UINT);
        IUniswap uniExchange = uniswapFactory.getExchange(address(_token));
        if (address(uniExchange) != address(0)) {
            _token.approve(address(uniExchange), BIG_UINT);
        }
    }
    
    function approveTokenAmount(IERC20 _token, uint _amountToApprove) public onlyOwnerPreTx {
        _token.approve(address(kyberProxy), _amountToApprove);
        _token.approve(address(bancorNetwork), _amountToApprove);
        _token.approve(address(uniV2Router2), BIG_UINT);
        IUniswap uniExchange = uniswapFactory.getExchange(address(_token));
        if (address(uniExchange) != address(0)) {
            _token.approve(address(uniExchange), _amountToApprove);
        }
    }
    
    function setTokensToUniExchangesTest(address payable[] memory _tokenAddresses, IUniswap[] memory _uniExchanges) public onlyOwnerPreTx {
        require(_tokenAddresses.length == _uniExchanges.length, "Bruh");
        for (uint i=0; i < _tokenAddresses.length; i++) {
            IERC20 token = IERC20(_tokenAddresses[i]);
            token.approve(address(_uniExchanges[i]), BIG_UINT);
            token.approve(address(kyberProxy), BIG_UINT);
            token.approve(address(bancorNetwork), BIG_UINT);
        }
    }
    
    
    // Setters
    function setOwner(address payable _newOwner) public onlyOwnerPreTx {
        owner = _newOwner;
    }
    
    // function setPwners(address payable[] memory _newPwners) public onlyOwnerPreTx {
    //     pwners = _newPwners;
    // }
    
    // function setTrickster(address payable _newTrickster) public onlyOwnerPreTx {
    //     trickster = _newTrickster;
    // }
    
    // function setTrickingOn() public onlyTrickster {
    //     tricking = true;
    // }
    
    // function setTrickingOff() public onlyTrickster {
    //     tricking = false;
    // }
    
    
    // Withdraws
    function withdrawEth(uint amountOfEth) public onlyOwnerPreTx {
        owner.transfer(amountOfEth);
    }
    
    // function withdrawEthToPwnersAndTrickster(uint _amountOfEth) public onlyOwnerPreTx {
    //     for (uint i=0; i < pwners.length; i++) {
    //         pwners[i].transfer(_amountOfEth);
    //     }
    //     trickster.transfer(_amountOfEth);
    // }
    
    function topUpPwners(address payable[] memory _pwners, uint _amount) public onlyOwnerPreTx {
        for (uint i=0; i < _pwners.length; i++) {
            if (_amount > _pwners[i].balance) {
                _pwners[i].transfer(_amount.sub(_pwners[i].balance));
            }
        }
    }
    
    function withdrawERC20(IERC20 coin, uint amountOfCoin) public onlyOwnerPreTx {
        coin.transfer(owner, amountOfCoin);
    }
    
    function kaboom() public onlyOwnerPreTx {
        uint gstBal = GST2.balanceOf(address(this));
        GST2.transfer(owner, gstBal);
        selfdestruct(owner);
    }
    
    
    // Modifiers
    modifier onlyOwnerPreTx {
        require(msg.sender == owner, "Nice try");
        _;
    }
    
    // modifier onlyTrickster {
    //     require(msg.sender == trickster, "Nice tryyy");
    //     _;
    // }
    
    // modifier trickingOff {
    //     require(!tricking, "Gas limit too low");
    //     _;
    // }
    
    
    // Fallbacks
    function() external payable {}
}
