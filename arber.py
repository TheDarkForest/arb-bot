import time
import datetime
from web3 import Web3, IPCProvider, WebsocketProvider, exceptions
import logging
import addresses
import math
import ABIs
import traceback
from scipy.optimize import minimize_scalar
import sqlite3 as lite
import subprocess
import random



def now():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def log(text, level):
    text = '{}\n'.format(text)
    print(text)
    if level == 'debug':
        logger.debug(text)
    if level == 'info':
        logger.info(text)
    if level == 'warning':
        logger.warning(text)
    if level == 'error':
        logger.error(text)
    if level == 'critical':
        logger.critical(text)


def cleanTxForTrace(tx):
    cleanTx = {}
    cleanTx['from'] = tx['from']
    cleanTx['to'] = tx['to']
    cleanTx['gas'] = W3.toHex(tx['gas'])
    cleanTx['gasPrice'] = W3.toHex(tx['gasPrice'])
    cleanTx['value'] = W3.toHex(tx['value'])
    if 'data' in tx:
        cleanTx['data'] = tx['data']
    if 'input' in tx:
        cleanTx['data'] = tx['input']
    return cleanTx


def searchMempool():
    def getTxsAndCheck(targetTxHashes, failedTx):

        def handleKnownException(targetTxHash, failedTx, msg):
            global getTxFailedCntr
            global getTxFailFailCntr
            getTxFailedCntr += 1
            if failedTx: getTxFailFailCntr += 1
            # log('{} error = {}'.format(targetTxHash.hex(), e), 'error')
            # print(e)
            if failedTx:
                log('Couldn\'t do getTransaction on {}: {}'.format(targetTxHash.hex(), msg), 'warning')
            else:
                failedLookupTargetTxHashes.append(targetTxHash)

        global getTxCntr
        global getTxAttemptsSingleRound
        getTxAttemptsSingleRound = 0
        global getTxTimeoutCntr
        global getTxFailedCntr
        global getTxFailSuccessCntr
        global getTxFailFailCntr
        getTxsAndCheckStartTime = time.time()
        heightStart = W3.eth.blockNumber

        for targetTxHash in targetTxHashes:
            try:
                tempTime2 = time.time()
                getTxAttemptsSingleRound += 1
                # targetTx = W3.eth.getTransaction(targetTxHash)
                targetTx = W3Timeout.eth.getTransaction(targetTxHash)
                # print(targetTx, '\n')
                if failedTx: getTxFailSuccessCntr += 1
                getTxCntr += 1
                if time.time() - tempTime2 > 0.3:
                    getTxTimeoutCntr += 1
                    # print(targetTx, '\n')
                # Analysis that results in an error for non-getTransaction reasons will repeat once
                checkPendingTx(targetTx)
            except exceptions.TransactionNotFound as e:
                handleKnownException(targetTxHash, failedTx, 'not found')
            except Exception as e:
                # Don't have access to the exception itself
                if 'Timeout' in str(type(e)):
                    handleKnownException(targetTxHash, failedTx, 'timeout')
                else:
                    raise(e)

        if time.time() - getTxsAndCheckStartTime > 2:
            log('heightStart = {}, heightNow = {}'.format(heightStart, W3.eth.blockNumber), 'info')


    def checkPendingTx(targetTx):
        if targetTx.gas < 50000:
            return


        if not productionMode and targetTx.to not in CONTRACTS_TO_TEST_WITH:
            return

        if targetTx.to is None or targetTx.to in addressesToIgnore:
            return

        if targetTx['from'] in addresses.pwners or targetTx['from'] == TRICKSTER_ADDRESS:
            return

        if len(targetTx.input) <= 10:
            return

        # if targetTx.to not in ALL_EXCHANGES_SET:
        #     # log('targetTx not a regular user targetTx. Too risky', 'warning')
        #     return

        checkForTrade(targetTx)

    pendingTargetTxHashes = PENDING_TXS_FILTER.get_new_entries()
    failedLookupTargetTxHashes = []

    getTxsAndCheck(pendingTargetTxHashes, False)
    # if len(failedLookupTargetTxHashes) > 0:
    #     time.sleep(0.5)
    #     getTxsAndCheck(failedLookupTargetTxHashes, True)


def checkForTrade(targetTx):
    def searchTraceForExchanges(fullTrace, exNamesToSearchFor):
        for call in fullTrace.trace:
            if 'callType' in call.action:
                if call.action.callType == 'call' and len(call.action.input) > 10:
                    # if call.action.to not in fullTrace.stateDiff:
                    #     log('Exchange {} balance not modified in stateDif. Tx likely reverted.'.format(call.action.to), 'warning')
                    #     log('tx = {}'.format(tx), 'warning')
                    #     return

                    # # This check is likely obsolete as it should be true whenever the above stateDif check is true
                    if 'error' in call:
                        # log('Error found in tx', 'warning')
                        # log('tx = {}'.format(targetTx), 'warning')
                        # print()
                        # print(fullTrace.trace)
                        # print()
                        return

                    # UniswapV1
                    if UNIV1 in exNamesToSearchFor and call.action.to in addresses.uniV1ExsLower:
                        log('Searched for {}'.format(exNamesToSearchFor), 'info')
                        exAddr = W3.toChecksumAddress(call.action.to)
                        ex = uniV1ExchangeAddressesToContracts[exAddr]
                        decodedInputFull = ex.decode_function_input(call.action.input)
                        log('decodedInputFull = {}'.format(decodedInputFull), 'info')
                        tokenAddr = uniV1ExchangeAddressesToTokenAddresses[exAddr]

                        if str(decodedInputFull[0]) in SUPPORTED_UNISWAPV1_FUNCTIONS_FULL[0:4]:
                            findBestTrade(targetTx, UNIV1, tokenAddr, True, fullTrace, None)

                        elif str(decodedInputFull[0]) in SUPPORTED_UNISWAPV1_FUNCTIONS_FULL[4:8]:
                            findBestTrade(targetTx, UNIV1, tokenAddr, False, fullTrace, None)

                        elif str(decodedInputFull[0]) in SUPPORTED_UNISWAPV1_FUNCTIONS_FULL[8:10]:
                            findBestTrade(targetTx, UNIV1, tokenAddr, False, fullTrace, None)
                            findBestTrade(targetTx, UNIV1, decodedInputFull[1]['token_addr'], True, fullTrace, None)

                        elif str(decodedInputFull[0]) in SUPPORTED_UNISWAPV1_FUNCTIONS_FULL[10:12]:
                            findBestTrade(targetTx, UNIV1, tokenAddr, False, fullTrace, None)
                            findBestTrade(targetTx, UNIV1, decodedInputFull[1]['token_addr'], True, fullTrace, None)

                        else:
                            log('Not a supported method', 'warning')

                        # We cover all cases for Uniswap above so can return here
                        return


                    # Kyber
                    # Could use trade with KyberContract2 instead?
                    elif KYBER in exNamesToSearchFor and call.action.to == KYBER_PROXY_ADDRESS_LOWER and productionMode:
                        log('Searched for {}'.format(exNamesToSearchFor), 'info')
                        decodedInputFull = KYBER_PROXY.decode_function_input(call.action.input)
                        log('decodedInputFull = {}'.format(decodedInputFull), 'info')
                        decodedInputs = decodedInputFull[1]
                        if str(decodedInputFull[0]) in SUPPORTED_KYBER_FUNCTIONS_FULL:
                            # log('stateDiff = {}'.format(fullTrace.stateDiff), 'info')

                            if decodedInputs['src'] == EX_NAME_TO_ETH_TOKEN_ADDR[KYBER]:
                                findBestTrade(targetTx, KYBER, decodedInputs['dest'], True, fullTrace, None)

                            elif decodedInputs['dest'] == EX_NAME_TO_ETH_TOKEN_ADDR[KYBER]:
                                findBestTrade(targetTx, KYBER, decodedInputs['src'], False, fullTrace, None)

                            else:
                                findBestTrade(targetTx, KYBER, decodedInputs['src'], False, fullTrace, None)
                                findBestTrade(targetTx, KYBER, decodedInputs['dest'], True, fullTrace, None)

                            # We don't cover cases like a user calling trade(), so can only return under supported fcns
                            return

                    # Bancor
                    elif BANCOR in exNamesToSearchFor and call.action.to == BANCOR_ADDRESS_LOWER:
                        log('Searched for {}'.format(exNamesToSearchFor), 'info')
                        decodedInputFull = BANCOR_PROXY.decode_function_input(call.action.input)
                        log('decodedInputFull = {}'.format(decodedInputFull), 'info')
                        decodedInputs = decodedInputFull[1]
                        if 'getReturnByPath' not in str(decodedInputFull[0]) and '_path' in decodedInputs and '_amount' in decodedInputs:
                            path = decodedInputs['_path']
                            print(path)
                            if path[0] == EX_NAME_TO_ETH_TOKEN_ADDR[BANCOR]:
                                findBestTrade(targetTx, BANCOR, path[-1], True, fullTrace, None)

                            elif path[-1] == EX_NAME_TO_ETH_TOKEN_ADDR[BANCOR]:
                                findBestTrade(targetTx, BANCOR, path[0], False, fullTrace, None)

                            else:
                                findBestTrade(targetTx, BANCOR, path[0], False, fullTrace, None)
                                findBestTrade(targetTx, BANCOR, path[-1], True, fullTrace, None)

                            return


                    # UniswapV2
                    if UNIV2 in exNamesToSearchFor and call.action.to in UNIV2_ROUTER_ADDR_LOWER_TO_CONTRACT:
                        log('Searched for {}'.format(exNamesToSearchFor), 'info')
                        router = UNIV2_ROUTER_ADDR_LOWER_TO_CONTRACT[call.action.to]
                        decodedInputFull = router.decode_function_input(call.action.input)
                        log('decodedInputFull = {}'.format(decodedInputFull), 'info')
                        decodedInputs = decodedInputFull[1]
                        if 'path' in decodedInputs:
                            uniV2Path = decodedInputs['path']
                            for i in range(len(uniV2Path)-1):
                                srcTokenAddr = uniV2Path[i]
                                destTokenAddr = uniV2Path[i+1]

                                if srcTokenAddr == EX_NAME_TO_ETH_TOKEN_ADDR[UNIV2]:
                                    findBestTrade(targetTx, UNIV2, destTokenAddr, True, fullTrace, [destTokenAddr, srcTokenAddr])
                                elif destTokenAddr == EX_NAME_TO_ETH_TOKEN_ADDR[UNIV2]:
                                    findBestTrade(targetTx, UNIV2, srcTokenAddr, False, fullTrace, [destTokenAddr, srcTokenAddr])
                                else:
                                    # If someone trades DAI > MKR in a DAI/MKR contract, they're increasing the price of
                                    # MKR on that contract, so we should buy MKR somewhere else and sell it on the
                                    # contract, which means we'll receive DAI and have to sell that for ETH. It also
                                    # reduces the price of DAI, so we should buy DAI there and sell it somewhere else,
                                    # which requires trading MKR for. So in both cases, the token that needs to be bought
                                    # from other exchanges is MKR

                                    findBestTrade(targetTx, UNIV2, destTokenAddr, True, fullTrace, getBestUniV2CommonPath([destTokenAddr, srcTokenAddr], False))
                            # TODO: check that e.g. Dai isn't used twice when extending the path. Dai > Dai > Eth would fail
                            #  Or maybe it's fine if that fails cause Dai > Eth would also be tried and would work
                            #  But we do need to check whether the last part of the path is eth because adding anything to that
                            #  would always fail

                            # TODO: If we buy a token from somewhere in order to sell it to a token/token contract in
                            #  uniV2, then it might be feasible to make a 3rd trade of tokenReceived > ETH on another
                            #  exchange, since it doesn't have to be uniV2, but that would complicate things alot more
                            return


    cleanTx = cleanTxForTrace(targetTx)
    startTimeToTrace = time.time()
    try:
        fullTrace = W3.manager.request_blocking("trace_call", [cleanTx, ['trace', 'stateDiff'], "latest"])
    except Exception as e:
        log('Couldn\'t get trace', 'warning')
        log('tx = {}'.format(targetTx), 'warning')
        log('Err = {}'.format(e), 'warning')
        return
    if time.time() - startTimeToTrace > TRACE_TIME_TO_FLAG:
        log('Tx {} took >{}s to trace'.format(W3.toHex(targetTx.hash), TRACE_TIME_TO_FLAG), 'warning')


    if any(uniExAddr in fullTrace.stateDiff for uniExAddr in addresses.uniV2ExsLower):
        searchTraceForExchanges(fullTrace, [UNIV2])
    elif any(uniExAddr in fullTrace.stateDiff for uniExAddr in addresses.uniV1ExsLower):
        searchTraceForExchanges(fullTrace, [UNIV1])
    else:
        searchTraceForExchanges(fullTrace, EX_NAMES_TO_IDS)

    return


def getBestUniV2CommonPath(startingPath, isEthToToken):
    pathExtensions = UNIV2_PATH_EXTENSIONS_ETH_TO_TOKEN if isEthToToken else UNIV2_PATH_EXTENSIONS_TOKEN_TO_ETH
    ethOutMax = 0
    bestPath = []
    for pathExtension in pathExtensions:
        path = pathExtension + startingPath if isEthToToken else startingPath + pathExtension
        log('path = {}'.format(path), 'info')
        try:
            amountsOut = UNIV2_ROUTER2.functions.getAmountsOut(TEN_18, path).call()
            log('amountsOut = {}'.format(amountsOut), 'info')
            ethOut = amountsOut[-1]
            if ethOut > ethOutMax:
                ethOutMax = ethOut
                bestPath = path
        except Exception as e:
            log('Couldn\'t do getAmountsOut: {}'.format(e), 'info')

    return bestPath


def findBestTrade(targetTx, targetExName, tokenAddr, targetBoughtToken, originalFullTrace, uniV2Path):
    if tokenAddr in tokenAddressesNoAllowance:
        log('tokenAddr {} is in tokenAddressesNoAllowance'.format(tokenAddr), 'info')
        return

    # TODO: remove
    if tokenAddr == '0x8E870D67F660D95d5be530380D0eC0bd388289E1':
        log('Ignoring PAX', 'info')
        return

    if tokenAddr not in TRADABLE_TOKENS:
        log('tokenAddr {} is not in TRADABLE_TOKENS'.format(tokenAddr), 'info')
        return

    maxEthInputAvailable = W3.eth.getBalance(TRADER_ADDRESS)
    log('targetTx = {}, token = {} targetExchangeName = {}'.format(targetTx, tokenAddr, targetExName), 'info')
    targetTxClean = cleanTxForTrace(targetTx)
    targetExID = EX_NAMES_TO_IDS[targetExName]
    targetBalTo = W3.toInt(hexstr=originalFullTrace.stateDiff[targetTx['from'].lower()]['balance']['*']['to'])

    bancorPath = []
    if tokenAddr in BANCOR_TOKEN_ADDRS_SET:
        if targetBoughtToken and targetExName == BANCOR:
            bancorPathSrcAddr, bancorPathDestAddr = tokenAddr, EX_NAME_TO_ETH_TOKEN_ADDR[BANCOR]
        elif targetBoughtToken and targetExName != BANCOR:
            bancorPathSrcAddr, bancorPathDestAddr = EX_NAME_TO_ETH_TOKEN_ADDR[BANCOR], tokenAddr
        elif not targetBoughtToken and targetExName == BANCOR:
            bancorPathSrcAddr, bancorPathDestAddr = EX_NAME_TO_ETH_TOKEN_ADDR[BANCOR], tokenAddr
        elif not targetBoughtToken and targetExName != BANCOR:
            bancorPathSrcAddr, bancorPathDestAddr = tokenAddr, EX_NAME_TO_ETH_TOKEN_ADDR[BANCOR]
        bancorPathStartTime = time.time()
        log('bancorPathSrcAddr = {} bancorPathDestAddr = {}'.format(bancorPathSrcAddr, bancorPathDestAddr), 'info')
        bancorPath = subprocess.run(['node', 'getBancorPath.js', bancorPathSrcAddr, bancorPathDestAddr], stdout=subprocess.PIPE).stdout.decode('utf-8').splitlines()
        log('bancorPathTime = {}, bancorPath = {}'.format(time.time()-bancorPathStartTime, bancorPath), 'info')
        if targetExName == BANCOR and bancorPath[0] == 'err':
            return

    if targetExName != UNIV2:
        uniV2Path = getBestUniV2CommonPath([tokenAddr], targetBoughtToken)
    log('uniV2Path = {}'.format(uniV2Path), 'info')


    def getProfitForTrade(srcExIn):
        # log('--------srcExIn = {} = {}--------'.format(srcExIn, srcExIn/TEN_18), 'info')
        # Want it to start from a low value
        srcExIn = int(upperBound - srcExIn)
        # log('srcExIn inversed = {} = {}'.format(srcExIn, srcExIn/TEN_18), 'info')
        # print(targetExID)
        # print(otherExID)
        if targetBoughtToken:
            arbTx = TRADER.functions.oneXTest(
                targetTx['from'],
                targetBalTo,
                0,
                0,
                targetExID,
                otherExID,
                TEN_18,
                1,
                tokenAddr,
                bancorPath,
                uniV2Path,
                srcExIn
            ).buildTransaction({'from': OWNER_ADDR, 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE, 'chainId': 1, 'gasPrice': targetTx.gasPrice})
            # nonce = W3.eth.getTransactionCount(addresses.pwners[0])
            # arbTxDebug = TRADER.functions.oneX(
            #     targetTx['from'],
            #     targetBalTo,
            #     0,
            #     0,
            #     targetExID,
            #     otherExID,
            #     TEN_18,
            #     1,
            #     1,
            #     tokenAddr,
            #     bancorPath,
            #     srcExIn
            # ).buildTransaction({
            #     'chainId': 1,
            #     'gas': 1000000,
            #     'gasPrice': targetTx.gasPrice-1,
            #     'nonce': nonce,
            # })

        else:
            srcExTestOut = BIG_UINT if targetExName == UNIV1 else 1
            arbTx = TRADER.functions.twoOTest(
                targetTx['from'],
                targetBalTo,
                0,
                0,
                targetExID,
                otherExID,
                srcExTestOut,
                tokenAddr,
                bancorPath,
                uniV2Path,
                srcExIn
            ).buildTransaction({'from': OWNER_ADDR, 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE, 'chainId': 1, 'gasPrice': targetTx.gasPrice})
            # nonce = W3.eth.getTransactionCount(addresses.pwners[0])
            # arbTxDebug = TRADER.functions.twoO(
            #     targetTx['from'],
            #     targetBalTo,
            #     0,
            #     0,
            #     targetExID,
            #     otherExID,
            #     1,
            #     srcExTestOut,
            #     tokenAddr,
            #     bancorPath,
            #     srcExIn
            # ).buildTransaction({
            #     'chainId': 1,
            #     'gas': 1000000,
            #     'gasPrice': targetTx.gasPrice-1,
            #     'nonce': nonce,
            # })

        arbTxClean = cleanTxForTrace(arbTx)
        fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, ['trace', 'stateDiff']], [arbTxClean, ['trace', 'stateDiff']]], "latest"])

        if TRADER_ADDRESS_LOWER not in fullTrace[1]['stateDiff'] and i == 18:
            log('tx trace err in getProfitForTrade, fullTrace = {}'.format(fullTrace[1]), 'warning')
            log('targetTx[\'from\'] = {}, targetBalTo = {}, targetExID = {}, otherExID = {}, '
                'tokenAddr = {}, bancorPath = {}, uniV2Path = {}, srcExIn = {}'
                .format(targetTx['from'], targetBalTo, targetExID, otherExID, tokenAddr, bancorPath, uniV2Path, srcExIn), 'warning')
            # signSendTx(targetTx, 0, arbTxDebug, 'test0', addresses.pwnerPrivKeys[0])

        # print()
        # print(fullTrace[1]['stateDiff'])
        # print()
        profit = W3.toInt(hexstr=fullTrace[1]['stateDiff'][TRADER_ADDRESS_LOWER]['balance']['*']['to']) - W3.toInt(hexstr=fullTrace[1]['stateDiff'][TRADER_ADDRESS_LOWER]['balance']['*']['from'])
        # log('profit = {} = {}'.format(profit, profit/TEN_18), 'info')
        return profit

    def getNegProfitForTrade(srcExIn):
        return -getProfitForTrade(srcExIn)

    t0 = time.time()
    bestExchangeName = None
    maxSrcExIn, maxProfit = 0, 0

    for otherExchangeName in TOKEN_ADDRS_INTERSECTIONS[targetExName]:
        log('-----------otherExchangeName = {}'.format(otherExchangeName), 'info')
        if otherExchangeName == BANCOR and tokenAddr in BANCOR_TOKEN_ADDRS_SET and bancorPath[0] == 'err':
            continue
        if targetExName == UNIV1 and otherExchangeName == UNIV1:
            continue
        if targetExName == BANCOR and otherExchangeName == BANCOR:
            continue
        if targetExName == UNIV2 and otherExchangeName == UNIV2:
            continue
        if tokenAddr not in TOKEN_ADDRS_INTERSECTIONS[targetExName][otherExchangeName]:
            log('{} not in intersection of supported tokens between {} and {}'.format(tokenAddr, targetExName, otherExchangeName), 'info')
            continue
        otherExID = EX_NAMES_TO_IDS[otherExchangeName]
        result = None
        for i in range(len(str(maxEthInputAvailable)), 17, -1):
            log(i, 'info')
            upperBound = min(10**i, maxEthInputAvailable)
            try:
                result = minimize_scalar(
                    getNegProfitForTrade,
                    bounds=(TEN_15, upperBound),
                    method='bounded',
                    tol=TEN_13
                )
                break
            except Exception as e:
                log('Err: {}'.format(e), 'warning')

        log('time for finding max profit = {}'.format(time.time() - t0), 'info')
        if not result or not result.success:
            log('Problem in minimize_scalar with {} and {}. Result = {}'.format(targetExName, otherExchangeName, result), 'info')
            continue
        if result.fun > 0:
            log('Price not moved enough to profit from {}'.format(otherExchangeName), 'info')
            continue
        maxSrcExInForIntersection = int(upperBound - result.x)
        maxProfitForIntersection = int(-result.fun)
        log('ethInputFromScipy = {}'.format(result), 'info')

        log('maxSrcExInForIntersection = {} = {}'.format(maxSrcExInForIntersection, maxSrcExInForIntersection/TEN_18), 'info')
        log('maxProfitForIntersection = {} = {}'.format(maxProfitForIntersection, maxProfitForIntersection/TEN_18), 'info')

        if maxProfitForIntersection > maxProfit:
            bestExchangeName = otherExchangeName
            maxSrcExIn = maxSrcExInForIntersection
            maxProfit = maxProfitForIntersection

    if not bestExchangeName:
        log('Price not moved enough to profit', 'info')
        return
    srcExIn = maxSrcExIn
    profit = maxProfit

    log('targetExchangeName = {}'.format(targetExName), 'info')
    log('bestExchangeName = {}'.format(bestExchangeName), 'info')
    log('targetBoughtToken = {}'.format(targetBoughtToken), 'info')
    log('srcExIn = {} = {}'.format(srcExIn, srcExIn/TEN_18), 'info')
    log('profit = {} = {}'.format(profit, profit/TEN_18), 'info')

    # Find the numbers to use as tests for the trade with the most profit
    try:
        srcExTestOut, srcExOut, destExTestOut = None, None, None
        if targetBoughtToken:
            if bestExchangeName == UNIV1:
                srcExOutTxClean = cleanTxForTrace(uniV1TokenAddressesToExchangeContracts[tokenAddr].functions.ethToTokenSwapInput(1, DEADLINE).buildTransaction({'value': srcExIn, 'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, ['stateDiff']], [srcExOutTxClean, ['trace', 'stateDiff']]], "latest"])
                # print('\nflibble')
                # print(fullTrace, '\n')
                # srcExTestOut = W3.toInt(hexstr=fullTrace[1]['stateDiff'][uniV1TokenAddressesToExchangeContracts[tokenAddr].address.lower()]['balance']['*']['from'])
                srcExOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])

            elif bestExchangeName == KYBER:
                srcExTestOutTxClean = cleanTxForTrace(KYBER_PROXY.functions.getExpectedRate(EX_NAME_TO_ETH_TOKEN_ADDR[bestExchangeName], tokenAddr, srcExIn).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                srcExOutTxClean = cleanTxForTrace(KYBER_PROXY.functions.trade(EX_NAME_TO_ETH_TOKEN_ADDR[bestExchangeName], srcExIn, tokenAddr, TRADER_ADDRESS, BIG_UINT, 1, TRADER_ADDRESS).buildTransaction({'value': srcExIn, 'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [srcExTestOutTxClean, []], [srcExOutTxClean, ['stateDiff']]], "latest"])
                # srcExTestOut = int(0.998 * W3.toInt(hexstr=fullTrace[1]['output'][0:66]))
                srcExOut = W3.toInt(hexstr=fullTrace[2]['output'][0:66])
                # log('stateDiff = {}'.format(fullTrace[2]['stateDiff']), 'info')

            elif bestExchangeName == BANCOR:
                srcExTestOutTxClean = cleanTxForTrace(BANCOR_PROXY.functions.getReturnByPath(bancorPath, srcExIn).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                srcExOutTxClean = cleanTxForTrace(BANCOR_PROXY.functions.convert(bancorPath, srcExIn, 1).buildTransaction({'value': srcExIn, 'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [srcExTestOutTxClean, []], [srcExOutTxClean, ['stateDiff']]], "latest"])
                # srcExTestOut = int(W3.toInt(hexstr=fullTrace[1]['output'][0:66]))
                srcExOut = W3.toInt(hexstr=fullTrace[2]['output'][0:66])
            elif bestExchangeName == UNIV2:
                srcExOutTxClean = cleanTxForTrace(UNIV2_ROUTER2.functions.getAmountsOut(srcExIn, uniV2Path).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [srcExOutTxClean, []]], "latest"])
                print(fullTrace[1]['output'])
                srcExOut = W3.toInt(hexstr=fullTrace[1]['output'][-64:])
                print('srcExOut =', srcExOut)



            if targetExName == UNIV1:
                destExTestOut = W3.toInt(hexstr=originalFullTrace.stateDiff[uniV1TokenAddressesToExchangeContracts[tokenAddr].address.lower()]['balance']['*']['to'])

            elif targetExName == KYBER:
                destExTestOutTxClean = cleanTxForTrace(KYBER_PROXY.functions.getExpectedRate(tokenAddr, EX_NAME_TO_ETH_TOKEN_ADDR[targetExName], srcExOut).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [destExTestOutTxClean, []]], "latest"])
                destExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])

            elif targetExName == BANCOR:
                destExTestOutTxClean = cleanTxForTrace(BANCOR_PROXY.functions.getReturnByPath(bancorPath, srcExOut).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [destExTestOutTxClean, []]], "latest"])
                destExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])
            elif targetExName == UNIV2:
                destExTestOutTxClean = cleanTxForTrace(UNIV2_ROUTER2.functions.getAmountsOut(srcExOut, uniV2Path).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [destExTestOutTxClean, []]], "latest"])
                print(fullTrace[1]['output'])
                destExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][-64:])
                print('destExTestOut =', destExTestOut)

        else:
            if targetExName == UNIV1:
                srcExOutTxClean = cleanTxForTrace(uniV1TokenAddressesToExchangeContracts[tokenAddr].functions.getEthToTokenInputPrice(srcExIn).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [srcExOutTxClean, []]], "latest"])
                srcExTestOut = W3.toInt(hexstr=originalFullTrace.stateDiff[uniV1TokenAddressesToExchangeContracts[tokenAddr].address.lower()]['balance']['*']['to'])
                # srcExOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])

            elif targetExName == KYBER:
                srcExTestOutTxClean = cleanTxForTrace(KYBER_PROXY.functions.getExpectedRate(EX_NAME_TO_ETH_TOKEN_ADDR[targetExName], tokenAddr, srcExIn).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                srcExOutTxClean = cleanTxForTrace(KYBER_PROXY.functions.trade(EX_NAME_TO_ETH_TOKEN_ADDR[targetExName], srcExIn, tokenAddr, TRADER_ADDRESS, BIG_UINT, 1, TRADER_ADDRESS).buildTransaction({'value': srcExIn, 'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [srcExTestOutTxClean, []], [srcExOutTxClean, []]], "latest"])
                srcExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])
                # srcExOut = W3.toInt(hexstr=fullTrace[2]['output'][0:66])

            elif targetExName == BANCOR:
                srcExTestOutTxClean = cleanTxForTrace(BANCOR_PROXY.functions.getReturnByPath(bancorPath, srcExIn).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                srcExOutTxClean = cleanTxForTrace(BANCOR_PROXY.functions.convert(bancorPath, srcExIn, 1).buildTransaction({'value': srcExIn, 'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [srcExTestOutTxClean, []], [srcExOutTxClean, ['stateDiff']]], "latest"])
                srcExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])
                # srcExOut = W3.toInt(hexstr=fullTrace[2]['output'][0:66])
            elif targetExName == UNIV2:
                srcExOutTxClean = cleanTxForTrace(UNIV2_ROUTER2.functions.getAmountsOut(srcExIn, uniV2Path).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
                fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [srcExOutTxClean, []]], "latest"])
                print(fullTrace[1]['output'])
                srcExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][-64:])
                print('srcExOut =', srcExTestOut)


            # if bestExchangeName == KYBER:
            #     destExTestOutTxClean = cleanTxForTrace(KYBER_PROXY.functions.getExpectedRate(tokenAddr, EX_NAME_TO_ETH_TOKEN_ADDR[bestExchangeName], srcExOut).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
            #     fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [destExTestOutTxClean, []]], "latest"])
            #     destExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])
            #
            # elif bestExchangeName == UNIV1:
            #     destExTestOutTxClean = cleanTxForTrace(uniV1TokenAddressesToExchangeContracts[tokenAddr].functions.getEthToTokenInputPrice(srcExIn).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
            #     fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [destExTestOutTxClean, []]], "latest"])
            #     destExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])
            #
            # elif bestExchangeName == BANCOR:
            #     destExTestOutTxClean = cleanTxForTrace(BANCOR_PROXY.functions.getReturnByPath(bancorPath, srcExOut).buildTransaction({'from': addresses.pwners[0], 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE}))
            #     fullTrace = W3.manager.request_blocking("trace_callMany", [[[targetTxClean, []], [destExTestOutTxClean, []]], "latest"])
            #     destExTestOut = W3.toInt(hexstr=fullTrace[1]['output'][0:66])

    except Exception as e:
        log('error in getting test numbers, fullTrace = {}'.format(fullTrace), 'error')
        raise(e)



    log('srcExTestOut = {}'.format(srcExTestOut), 'info')
    log('srcExOut = {}'.format(srcExOut), 'info')
    log('destExTestOut = {}'.format(destExTestOut), 'info')
    log('time for findBestTrade = {}'.format(time.time() - t0), 'info')
    if targetExName != BANCOR and bestExchangeName != BANCOR:
        bancorPath = []
        log('bancorPath = {}'.format(bancorPath), 'info')
    if targetExName != UNIV2 and bestExchangeName != UNIV2:
        uniV2Path = []
        log('uniV2Path = {}'.format(uniV2Path), 'info')
    executeTrade(targetTx, EX_NAMES_TO_IDS[targetExName], EX_NAMES_TO_IDS[bestExchangeName], targetBoughtToken, tokenAddr, bancorPath, srcExIn, srcExTestOut, srcExOut, destExTestOut, profit, uniV2Path, targetBalTo)


def executeTrade(targetTx, targetExID, otherExID, targetBoughtToken, tokenAddr, bancorPath, srcExIn, srcExTestOut, srcExOut, destExTestOut, profit, uniV2Path, targetBalTo):
    def checkTxMined(txHash):
        try:
            receipt = W3.eth.getTransactionReceipt(txHash)
            log('Tx was already mined :( receipt = {}'.format(receipt), 'info')
            return True
        except exceptions.TransactionNotFound:
            pass
        return False

    if checkTxMined(targetTx.hash):
        return
    if profit/TEN_18 > 0.01:
        log('Decent profit here', 'info')

    startTimeExecuteTrade = time.time()
    global timeToCleanUp
    global gasPriceOfMostRecentTx
    gasPriceLastClone = targetTx.gasPrice-1 if targetTx.gasPrice > MIN_GAS_PRICE else MIN_GAS_PRICE
    chainParams = {
        'chainId': 1,
        'gasPrice': targetTx.gasPrice,
    }
    # clearMinedTxs()
    # pwnerPrivKeysToNonces = {}
    # for privKey in addresses.pwnerPrivKeys:
    #     # pendingTxs = len(pwnerAddressesToPendingTxs[pwnerPrivKeysToAddresses[privKey]].keys())
    #     # pwnerPrivKeysToNonces[privKey] = W3.eth.getTransactionCount(pwnerPrivKeysToAddresses[privKey]) + pendingTxs
    #     pwnerPrivKeysToNonces[privKey] = W3.eth.getTransactionCount(pwnerPrivKeysToAddresses[privKey])

    def estArbTxGas(isHit, numGSTs):
        if targetBoughtToken:
            destExTestOut = 1 if isHit else BIG_UINT
            arbTxGSTTest = TRADER.functions.oneXTest(
                '0x1100000000000000000000000000000000000011',
                0,
                numGSTs,
                numGSTs,
                targetExID,
                otherExID,
                srcExOut,
                destExTestOut,
                tokenAddr,
                bancorPath,
                uniV2Path,
                srcExIn
            ).buildTransaction({'from': OWNER_ADDR, 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE, 'chainId': 1, 'gasPrice': targetTx.gasPrice})
        else:
            if targetExID == EX_NAMES_TO_IDS[UNIV1]:
                srcExTestOut = BIG_UINT if isHit else 1
            else:
                srcExTestOut = 1 if isHit else BIG_UINT
            arbTxGSTTest = TRADER.functions.twoOTest(
                '0x1100000000000000000000000000000000000011',
                0,
                numGSTs,
                numGSTs,
                targetExID,
                otherExID,
                srcExTestOut,
                tokenAddr,
                bancorPath,
                uniV2Path,
                srcExIn
            ).buildTransaction({'from': OWNER_ADDR, 'gas': GAS_LIMIT_FOR_GAS_ESTIMATE, 'chainId': 1, 'gasPrice': targetTx.gasPrice})

        gas = W3.eth.estimateGas(arbTxGSTTest)
        return gas

    def getOptimalNumGSTs(gasLimit):
        if gasLimit < 40000:
            return 0
        # return int(math.ceil(gasLimit / 42300))
        optimalNumGSTs = int(round(gasLimit / 42300))
        GSTsAvailable = GST2.functions.balanceOf(TRADER_ADDRESS).call()
        return optimalNumGSTs if GSTsAvailable > optimalNumGSTs else GSTsAvailable

    def getNumGSTs(isHit, gasNoGSTs, optimalNumGSTs, gasGSTsRefunded):
        ethToMineGSTs = optimalNumGSTs * ETH_TO_MINE_GST2
        log('ethToMineGSTs = {} = {}'.format(ethToMineGSTs, ethToMineGSTs/TEN_18), 'info')
        ethRefunded = (gasNoGSTs - gasGSTsRefunded) * targetTx.gasPrice
        log('ethRefunded = {} = {}'.format(ethRefunded, ethRefunded/TEN_18), 'info')
        totalEthSaved = ethRefunded - ethToMineGSTs
        log('totalEthSaved = {} = {}'.format(totalEthSaved, totalEthSaved/TEN_18), 'info')
        optimalNumGSTs = 0 if totalEthSaved < 0 else optimalNumGSTs

        return optimalNumGSTs

    # def getNumGSTsForTargetTxHit(gasNoGSTs, hit):
    #     numGSTs = getNumGSTs(hit, gasNoGSTs)
    #     log('numGSTs = {}'.format(numGSTs), 'info')
    #     return numGSTs

    def getNumClones(gasCloneHit, gasCloneMiss, gasPrice, profit):
        numClones = 0
        if any(result['numHits'] > 0 for result in pastCloneResults) and len(pastCloneResults) > MIN_NUM_CLONE_RESULTS:
            cumPotProfit, cumGasPrice, cumNumHits, cumNumMisses = 0, 0, 0, 0
            numResults = min(MAX_PAST_RESULTS_TO_USE, len(pastCloneResults))
            for result in pastCloneResults[-numResults:]:
                cumPotProfit += result['potProfit']
                cumGasPrice += result['gasPrice']
                cumNumHits += result['numHits']
                cumNumMisses += result['numMisses']

            log('gasCloneHit = {}, gasCloneMiss = {}'.format(gasCloneHit, gasCloneMiss), 'info')
            log('cumPotProfit = {} = {}'.format(cumPotProfit, cumPotProfit/TEN_18), 'info')
            log('cumGasPrice = {} = {}'.format(cumGasPrice, cumGasPrice/TEN_9), 'info')
            log('cumNumHits = {}'.format(cumNumHits), 'info')
            log('cumNumMisses = {}'.format(cumNumMisses), 'info')
            avgGasPrice = cumGasPrice / numResults
            log('avgGasPrice = {} = {}'.format(avgGasPrice, avgGasPrice/TEN_9), 'info')
            cumNumClones = cumNumHits + cumNumMisses
            log('cumNumClones = {}'.format(cumNumClones), 'info')
            hitProb = cumNumHits / numResults
            log('hitProb = {}'.format(hitProb), 'info')
            totalNumArbs = cumNumClones / hitProb
            log('totalNumArbs = {}'.format(totalNumArbs), 'info')
            totalNumOtherArbs = totalNumArbs - cumNumClones
            log('totalNumOtherArbs = {}'.format(totalNumOtherArbs), 'info')
            ratioOfPotProfit = profit / cumPotProfit
            log('ratioOfPotProfit = {}'.format(ratioOfPotProfit), 'info')
            ratioOfGasPrices = avgGasPrice / gasPrice
            log('ratioOfGasPrices = {}'.format(ratioOfGasPrices), 'info')
            numExpectedOtherArbs = totalNumOtherArbs * ratioOfPotProfit * ratioOfGasPrices
            log('numExpectedOtherArbs = {}'.format(numExpectedOtherArbs), 'info')

            # maxExpectedTotalProfit = 0
            maxTotalReturn = 0
            # Somewhat of a sanity check?
            for i in range(1, len(addresses.pwners)+1):
                # expectedHitProb = i / (numExpectedOtherArbs + i)
                # expectedProfit = profit * expectedHitProb
                # expectedHitCost = gasPrice * (gasCloneHit + ((i-1) * gasCloneMiss))
                # expectedMissCost = gasPrice * i * gasCloneMiss
                # expectedCost = (expectedHitCost * expectedHitProb) + (expectedMissCost * (1 - expectedHitProb))
                # # Encourage sending fewer txs even more than it already does since each subsequent tx has less bang for its buck
                # # expectedCost *= i/5
                # expectedTotalProfit = (expectedProfit - expectedCost) if productionMode else expectedProfit
                # if expectedTotalProfit > maxExpectedTotalProfit:
                #     maxExpectedTotalProfit = expectedTotalProfit
                #     numClones = i
                # log('{}  expectedHitProb = {} expectedProfit = {} expectedHitCost = {} expectedMissCost = {} expectedCost = {} expectedTotalProfit = {} maxExpectedTotalProfit = {}'
                #     .format(i, expectedHitProb, expectedProfit/TEN_18, expectedHitCost/TEN_18, expectedMissCost/TEN_18, expectedCost/TEN_18, expectedTotalProfit/TEN_18, maxExpectedTotalProfit/TEN_18), 'info')

                hitProb = i / (numExpectedOtherArbs + i)

                # Hit
                hitCost = gasPrice * (gasCloneHit + ((i - 1) * gasCloneMiss))
                hitReturn = hitProb * (profit - hitCost)

                # Miss
                missCost = gasPrice * i * gasCloneMiss
                missReturn = (1-hitProb) * (-missCost)
                missReturn *= 1.25
                # missReturn *= (1 + (i/5))

                totalReturn = hitReturn + missReturn
                if totalReturn > maxTotalReturn:
                    maxTotalReturn = totalReturn
                    numClones = i
                log('{}  hitProb = {} hitCost = {} hitReturn = {} missCost = {} missReturn = {} totalReturn = {} maxTotalReturn = {}'
                    .format(i, hitProb, hitCost/TEN_18, hitReturn/TEN_18, missCost/TEN_18, missReturn/TEN_18, totalReturn/TEN_18, maxTotalReturn/TEN_18), 'info')


        else:
            # expectedProfit = profit
            # expectedHitCost = gasPrice * (((DEFAULT_NUM_CLONES - 1) * gasCloneMiss) + gasCloneHit)
            # expectedMissCost = gasPrice * DEFAULT_NUM_CLONES * gasCloneMiss
            # expectedCost = (DEFAULT_CLONE_TRADES_HIT_PROB * expectedHitCost) + ((1 - DEFAULT_CLONE_TRADES_HIT_PROB) * expectedMissCost)
            # if expectedCost > profit and productionMode:

            # Hit
            hitCost = gasPrice * (gasCloneHit + ((DEFAULT_NUM_CLONES - 1) * gasCloneMiss))
            hitReturn = DEFAULT_CLONE_TRADES_HIT_PROB * (profit - hitCost)

            # Miss
            missCost = gasPrice * DEFAULT_NUM_CLONES * gasCloneMiss
            missReturn = (1-DEFAULT_CLONE_TRADES_HIT_PROB) * (-missCost)

            totalReturn = hitReturn + missReturn
            if totalReturn > 0:
                numClones = DEFAULT_NUM_CLONES
            else:
                numClones = 0

            log('{}  hitProb = {} hitCost = {} hitReturn = {} missCost = {} missReturn = {} totalReturn = {}'
                .format('Not enough previous trades yet.', DEFAULT_CLONE_TRADES_HIT_PROB, hitCost / TEN_18, hitReturn / TEN_18, missCost / TEN_18, missReturn / TEN_18,
                        totalReturn / TEN_18), 'info')

        log('numClones = {}'.format(numClones), 'info')
        if numClones == 0:
            log('Gas cost too high', 'info')

        return numClones

    isHit = False
    gasMissNoGSTs = EX_ID_TO_GAS_MISS[targetExID]
    log('-----------gasMissNoGSTs = {}-----------'.format(gasMissNoGSTs), 'info')
    missOptimalNumGSTs = getOptimalNumGSTs(gasMissNoGSTs)
    log('missOptimalNumGSTs = {}'.format(missOptimalNumGSTs), 'info')
    gasMissGSTs = int(max(gasMissNoGSTs - (NET_GAS_GAIN_GST2 * missOptimalNumGSTs), gasMissNoGSTs/2, 21000))
    log('gasMissGSTs = {}'.format(gasMissGSTs), 'info')

    # numGSTsTargetMiss = getNumGSTsForTargetTxHit(False)
    numGSTsTargetMiss = getNumGSTs(isHit, gasMissNoGSTs, missOptimalNumGSTs, gasMissGSTs)
    log('numGSTsTargetMiss = {}'.format(numGSTsTargetMiss), 'info')

    isHit = True
    gasHitNoGSTs = estArbTxGas(isHit, 0)
    log('-----------gasHitNoGSTs = {}-----------'.format(gasHitNoGSTs), 'info')
    hitOptimalNumGSTs = getOptimalNumGSTs(gasHitNoGSTs)
    log('hitOptimalNumGSTs = {}'.format(hitOptimalNumGSTs), 'info')
    gasHitGSTs = max(int(gasHitNoGSTs - (NET_GAS_GAIN_GST2 * hitOptimalNumGSTs)), int(gasHitNoGSTs/2))
    log('gasHitGSTs = {}'.format(gasHitGSTs), 'info')

    numGSTsTargetHit = getNumGSTs(isHit, gasHitNoGSTs, hitOptimalNumGSTs, gasHitGSTs)
    log('numGSTsTargetHit = {}'.format(numGSTsTargetHit), 'info')

    if targetBoughtToken:
        arbTx = TRADER.functions.oneX(
            targetTx['from'],
            targetBalTo,
            numGSTsTargetMiss,
            numGSTsTargetHit,
            targetExID,
            otherExID,
            srcExOut,
            destExTestOut,
            tokenAddr,
            bancorPath,
            uniV2Path,
            srcExIn
        )

    else:
        arbTx = TRADER.functions.twoO(
            targetTx['from'],
            targetBalTo,
            numGSTsTargetMiss,
            numGSTsTargetHit,
            targetExID,
            otherExID,
            srcExTestOut,
            tokenAddr,
            bancorPath,
            uniV2Path,
            srcExIn
        )

    chainParams['gas'] = int(gasHitNoGSTs * GAS_LIMIT_ESTIMATE_MULTIPLIER)
    print(chainParams['gas'])

    numClones = getNumClones(gasHitGSTs, gasMissGSTs, targetTx.gasPrice, profit) if productionMode else 5
    arbTx = arbTx.buildTransaction(chainParams)
    if checkTxMined(targetTx.hash): return

    for i, privKey in enumerate(random.sample(addresses.pwnerPrivKeys, numClones)):
        arbTx['nonce'] = W3.eth.getTransactionCount(pwnerPrivKeysToAddresses[privKey])
        # if i == numClones-1: arbTx['gasPrice'] = gasPriceLastClone
        signSendTx(targetTx, profit, arbTx, 'arbTx{}'.format(i), privKey)

    log('time for executeTrade() = {}\n'
        '-----------------------------------------------------------------------'
        '-----------------------------------------------------------------------'.format(time.time()-startTimeExecuteTrade), 'info')


def signSendTx(targetTx, profit, clone, name, privKey):
    t0 = time.time()
    signedTx = W3.eth.account.signTransaction(clone, private_key=privKey)
    log('{} = {}'.format(name, clone), 'info')
    print(time.time() - t0)

    t0 = time.time()
    try:
        result = W3.toHex(W3.eth.sendRawTransaction(signedTx.rawTransaction))
        addCloneToTargetTxHashesToClones(targetTx.hash, profit, targetTx.gasPrice, result)
        log('result {} = {}'.format(name, result), 'critical')
    except Exception as e:
        log('ERROR: {}'.format(e), 'error')
    print(time.time() - t0)

    # try:
    #     result = INFURA.eth.sendRawTransaction(signedTx.rawTransaction)
    #     log('result {} = {}'.format(name, W3.toHex(result)), 'critical')
    # except Exception as e:
    #     log('ERROR: {}'.format(e), 'error')

    # log('result {} = {}'.format(name, W3.toHex(result)), 'critical')
    # pwnerAddressesToPendingTxs[pwnerPrivKeysToAddresses[privKey]][W3.toHex(result)] = txDict


def addCloneToTargetTxHashesToClones(targetTxHash, profit, gasPrice, cloneHash):
    if targetTxHash in targetTxHashesToClones:
        targetTxHashesToClones[targetTxHash]['cloneHashes'].append(cloneHash)
    else:
        targetTxHashesToClones[targetTxHash] = {'potProfit': profit, 'gasPrice': gasPrice, 'cloneHashes': [cloneHash]}


def getBalAllSelf():
    total = 0
    # total += W3.eth.getBalance(TRICKSTER_ADDRESS)
    total += W3.eth.getBalance(TRADER_ADDRESS)
    for pwnerAddr in addresses.pwners:
        total += W3.eth.getBalance(pwnerAddr)
    return total


def updateTargetTxHashesToClones():
    targetTxHashesToDelete = []
    log('targetTxHashesToDelete = {}'.format(targetTxHashesToDelete), 'info')
    for targetTxHash, clonesInfo in targetTxHashesToClones.items():
        result = {}
        numHits, numMisses = 0, 0
        try:
            # Reversed so that it checks the one with the lowest gas price first, since if
            # that was mined, it's very likely the other clones were too
            for cloneHash in reversed(clonesInfo['cloneHashes']):
                receipt = W3.eth.getTransactionReceipt(cloneHash)
                log('len(receipt[\'logs\']) = {}, receipt = {}'.format(len(receipt['logs']), receipt), 'info')
                # Weird situation where a receipt is returned and it has been mined but
                if receipt['blockHash'] is None:
                    log('Tx {} has receipt but no block/log etc info ignoring batch temporarily {}'.format(cloneHash, receipt), 'info')
                    raise Exception('Message')
                # Reverted tx, ignore batch of clones
                if receipt['status'] == 0:
                    targetTxHashesToDelete.append(targetTxHash)
                    log('Found reverted {}, ignoring batch permanently'.format(cloneHash), 'info')
                    break
                if len(receipt['logs']) >= 2:
                    numHits += 1
                else:
                    numMisses += 1

            # TODO: get average of gasPrice and divide current gasPrice by cumGasPrice to better estimate
            #  the expected number of other arbers

            log('targetTxHashesToClones = {}'.format(targetTxHashesToClones), 'info')
            result['potProfit'] = clonesInfo['potProfit']
            result['gasPrice'] = clonesInfo['gasPrice']
            result['numHits'] = numHits
            result['numMisses'] = numMisses
            log('result = {}'.format(result), 'info')
            pastCloneResults.append(result)
            targetTxHashesToDelete.append(targetTxHash)
        except Exception as e:
            pass
            # log('Error = {}'.format(e), 'error')
    if len(targetTxHashesToDelete) > 0:
        log('targetTxHashesToDelete = {}'.format(targetTxHashesToDelete), 'info')
        for targetTxHash in targetTxHashesToDelete:
            del targetTxHashesToClones[targetTxHash]

        log('targetTxHashesToClones = {}'.format(targetTxHashesToClones), 'info')


def writeToCloneResultsDB():
    global cloneResultsDbIndex
    if len(pastCloneResults) > cloneResultsDbIndex:
        with lite.connect('cloneResults.db') as db:
            cur = db.cursor()
            resultsToWrite = [list(result.values()) for result in pastCloneResults[cloneResultsDbIndex:]]
            log('resultsToWrite = {}'.format(resultsToWrite), 'info')
            cur.executemany('INSERT INTO CloneResults VALUES (?,?,?,?)', resultsToWrite)
            cloneResultsDbIndex = len(pastCloneResults)



try:
    t0 = time.time()
    LOG_FILE_NAME = 'arber.log'
    VERBOSE_LOG_FILE_NAME = 'arberVerbose.log'

    logging.basicConfig(
        filename=VERBOSE_LOG_FILE_NAME,
        filemode='a',
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s     %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.INFO
    )
    logger = logging.getLogger('arber')
    log('-----------STARTING-----------', 'info')

    IPC_PATH = '/root/.local/share/openethereum/jsonrpc.ipc'
    W3 = Web3(IPCProvider(IPC_PATH))
    W3Timeout = Web3(IPCProvider(IPC_PATH, timeout=0.1))
    # TODO: WARNING, HAS BEEN USED FROM OLD PARITY MACHINE
    # INFURA = Web3(WebsocketProvider('REPLACE'))

    productionMode = True
    if not productionMode: log('WARNING! CURRENTLY IN TESTING MODE!', 'info')
    CONTRACTS_TO_TEST_WITH = []

    TRICKSTER_PRIVATE_KEY = 'REPLACE'
    TRICKSTER_ADDRESS = W3.toChecksumAddress('REPLACE')
    pwnerAddressesToPrivKeys = {}
    pwnerPrivKeysToAddresses = {}
    pwnerAddressesToPendingTxs = {}
    for i, addr in enumerate(addresses.pwners):
        pwnerAddressesToPrivKeys[addr] = addresses.pwnerPrivKeys[i]
        pwnerPrivKeysToAddresses[addresses.pwnerPrivKeys[i]] = addr
        pwnerAddressesToPendingTxs[addr] = {}

    # Clones
    targetTxHashesToClones = {}
    pastCloneResults = []
    MIN_NUM_CLONE_RESULTS = 3
    MAX_PAST_RESULTS_TO_USE = 100
    DEFAULT_NUM_CLONES = 5
    DEFAULT_CLONE_TRADES_HIT_PROB = 0.1
    CLONE_RESULTS_DB_NAME = 'cloneResults.db'
    cloneResultsDbIndex = 0
    with lite.connect(CLONE_RESULTS_DB_NAME) as db:
        cur = db.cursor()
        cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
        dbNames = cur.fetchall()
        # Do I need a 2nd cursor? No fucking clue. Seems unnecessary for them to even exist.
        cur = db.cursor()
    if len(dbNames) == 0 or 'CloneResults' not in dbNames[0]:
        log('Creating {}'.format(CLONE_RESULTS_DB_NAME), 'info')
        cur.execute("CREATE TABLE CloneResults(potProfit INT, gasPrice INT, numHits INT, numMisses INT)")
    else:
        cur.execute("SELECT * FROM CloneResults;")
        pastCloneResults += [{'potProfit': result[0], 'gasPrice': result[1], 'numHits': result[2], 'numMisses': result[3]} for result in cur.fetchall()]
        cloneResultsDbIndex += len(pastCloneResults)
        log('pastCloneResults = {}'.format(pastCloneResults), 'info')


    # ETH_TO_KEEP_IN_WALLET = W3.toWei(0.1, 'ether')
    ETH_TO_KEEP_IN_WALLET = 0
    MIN_GAS_PRICE = W3.toWei(1, 'gwei')
    # if productionMode:
    #     MIN_ETH = W3.toWei(0.1, 'ether')
    # else:
    #     MIN_ETH = 0
    global timeToCleanUp
    timeToCleanUp = time.time()
    global gasPriceOfMostRecentTx
    gasPriceOfMostRecentTx = 5*10**9
    TRACE_TIME_TO_FLAG = 0.25
    BIG_UINT = 2**254
    addressesToIgnore = set(addresses.toIgnore)
    TEN_18 = 10**18
    TEN_15 = 10**15
    TEN_13 = 10**13
    TEN_9 = 10**9


    # Bancor
    BANCOR = 'bancor'
    BANCOR_PROXY_ADDR = '0x3Ab6564d5c214bc416EE8421E05219960504eeAD'
    BANCOR_ADDRESS_LOWER = '0x3Ab6564d5c214bc416EE8421E05219960504eeAD'.lower()
    BANCOR_PROXY = W3.eth.contract(address=BANCOR_PROXY_ADDR, abi=ABIs.bancorABI)
    BANCOR_CONVERTER_REGISTRY_ADDRESS = '0xF84B332Db34C6A9b554D80cF9BC6124C1C74495D'
    BANCOR_CONVERTER_REGISTRY = W3.eth.contract(address=BANCOR_CONVERTER_REGISTRY_ADDRESS, abi=ABIs.bancorConverterRegistryABI)
    addresses.bancorTokens = BANCOR_CONVERTER_REGISTRY.functions.getConvertibleTokens().call()
    addresses.bancorTokensLower = [tokenAddr.lower() for tokenAddr in addresses.bancorTokens]
    BANCOR_TOKEN_ADDRS_SET = set(addresses.bancorTokens)
    print('len(BANCOR_TOKEN_ADDRESSES_SET) = {}'.format(len(BANCOR_TOKEN_ADDRS_SET)))


    # Kyber
    KYBER = 'kyber'
    KYBER_PROXY_ADDRESS = '0x818E6FECD516Ecc3849DAf6845e3EC868087B755'
    KYBER_PROXY_ADDRESS_LOWER = KYBER_PROXY_ADDRESS.lower()
    KYBER_PROXY = W3.eth.contract(address=KYBER_PROXY_ADDRESS, abi=ABIs.kyberProxyABI)
    addresses.kyberTokensLower = [tokenAddr.lower() for tokenAddr in addresses.kyberTokens]
    SUPPORTED_KYBER_FUNCTIONS_FULL = [
        '<Function tradeWithHint(address,uint256,address,address,uint256,uint256,address,bytes)>',
    ]
    KYBER_TOKEN_ADDRS_SET = set(addresses.kyberTokens)
    print('len(KYBER_TOKEN_ADDRESSES_SET) = {}'.format(len(KYBER_TOKEN_ADDRS_SET)))


    # UniswapV2
    UNIV2 = 'uniswapv2'
    UNIV2_FACTORY_ADDRESS = '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f'
    UNIV2_FACTORY = W3.eth.contract(address=UNIV2_FACTORY_ADDRESS, abi=ABIs.uniV2FactoryABI)
    UNIV2_ROUTER1_ADDR = '0xf164fC0Ec4E93095b804a4795bBe1e041497b92a'
    UNIV2_ROUTER1 = W3.eth.contract(address=UNIV2_ROUTER1_ADDR, abi=ABIs.uniV2Router1ABI)
    UNIV2_ROUTER2_ADDR = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
    UNIV2_ROUTER2 = W3.eth.contract(address=UNIV2_ROUTER2_ADDR, abi=ABIs.uniV2Router2ABI)
    UNIV2_ROUTER_ADDR_TO_CONTRACT = {UNIV2_ROUTER1_ADDR: UNIV2_ROUTER1, UNIV2_ROUTER2_ADDR: UNIV2_ROUTER2}
    UNIV2_ROUTER_ADDR_LOWER_TO_CONTRACT = {UNIV2_ROUTER1_ADDR.lower(): UNIV2_ROUTER1, UNIV2_ROUTER2_ADDR.lower(): UNIV2_ROUTER2}

    WETH_ADDR = '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2'
    DAI_ADDR = '0x6B175474E89094C44Da98b954EedeAC495271d0F'
    USDC_ADDR = '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48'
    UNIV2_PATH_EXTENSIONS_TOKEN_TO_ETH = [
        [WETH_ADDR],
        [DAI_ADDR, WETH_ADDR],
        [USDC_ADDR, WETH_ADDR],
    ]
    UNIV2_PATH_EXTENSIONS_ETH_TO_TOKEN = [
        [WETH_ADDR],
        [WETH_ADDR, DAI_ADDR],
        [WETH_ADDR, USDC_ADDR],
    ]

    uniV2ExAddrToContract = {}
    uniV2ExAddrToTokenAddrs = {}
    if productionMode:
        for i in range(UNIV2_FACTORY.functions.allPairsLength().call()):
            exAddr = UNIV2_FACTORY.functions.allPairs(i).call()
            uniV2ExAddrToContract[exAddr] = W3.eth.contract(address=exAddr, abi=ABIs.uniV2ExABI)
            uniV2ExAddrToTokenAddrs[exAddr] = (uniV2ExAddrToContract[exAddr].functions.token0().call(), uniV2ExAddrToContract[exAddr].functions.token1().call())
            for tokenAddr in uniV2ExAddrToTokenAddrs[exAddr]:
                if tokenAddr == '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE' or tokenAddr == WETH_ADDR:
                    continue
                if tokenAddr not in addresses.uniV2Tokens:
                    addresses.uniV2Tokens.append(tokenAddr)
    else:
        for tokenAddr in ['0x419c4dB4B9e25d6Db2AD9691ccb832C8D9fDA05E', '0x6B175474E89094C44Da98b954EedeAC495271d0F', '0x6710c63432A2De02954fc0f851db07146a6c0312']:
            addresses.uniV2Tokens.append(tokenAddr)

    addresses.uniV2TokensLower = [tokenAddr.lower() for tokenAddr in addresses.uniV2Tokens]
    UNIV2_TOKEN_ADDRS_SET = set(addresses.uniV2Tokens)
    print('len(UNISWAPV2_TOKEN_ADDRS_SET) = {}'.format(len(UNIV2_TOKEN_ADDRS_SET)))


    # UniswapV1
    UNIV1 = 'uniswapv1'
    DEADLINE = 2*10**9
    SUPPORTED_UNISWAPV1_FUNCTIONS_FULL = [
        '<Function ethToTokenSwapInput(uint256,uint256)>',
        '<Function ethToTokenTransferInput(uint256,uint256,address)>',
        '<Function ethToTokenSwapOutput(uint256,uint256)>',
        '<Function ethToTokenTransferOutput(uint256,uint256,address)>',

        '<Function tokenToEthSwapInput(uint256,uint256,uint256)>',
        '<Function tokenToEthTransferInput(uint256,uint256,uint256,address)>',
        '<Function tokenToEthSwapOutput(uint256,uint256,uint256)>',
        '<Function tokenToEthTransferOutput(uint256,uint256,uint256,address)>',

        '<Function tokenToTokenSwapInput(uint256,uint256,uint256,uint256,address)>',
        '<Function tokenToTokenTransferInput(uint256,uint256,uint256,uint256,address,address)>',
        '<Function tokenToTokenSwapOutput(uint256,uint256,uint256,uint256,address)>',
        '<Function tokenToTokenTransferOutput(uint256,uint256,uint256,uint256,address,address)>',
    ]
    UNIV1_FACTORY_ADDRESS = '0xc0a47dFe034B400B47bDaD5FecDa2621de6c4d95'
    UNIV1_FACTORY = W3.eth.contract(address=UNIV1_FACTORY_ADDRESS, abi=ABIs.uniV1FactoryABI)
    for tokenAddr in KYBER_TOKEN_ADDRS_SET | BANCOR_TOKEN_ADDRS_SET | UNIV2_TOKEN_ADDRS_SET:
        if tokenAddr == '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE':
            continue
        exAddr = UNIV1_FACTORY.functions.getExchange(tokenAddr).call()
        if exAddr != '0x0000000000000000000000000000000000000000' and W3.eth.getBalance(exAddr) > TEN_18:
            addresses.uniV1Tokens.append(tokenAddr)
            addresses.uniV1Exs.append(exAddr)
    addresses.uniV1TokensLower = [tokenAddr.lower() for tokenAddr in addresses.uniV1Tokens]
    UNIV1_TOKEN_ADDRS_SET = set(addresses.uniV1Tokens)
    print('len(UNISWAPV1_TOKEN_ADDRESSES_SET) = {}'.format(len(UNIV1_TOKEN_ADDRS_SET)))

    uniV1ExchangeAddressesToContracts = {}
    uniV1ExchangeAddressesToTokenAddresses = {}
    uniV1TokenAddressesToExchangeContracts = {}
    for i, uniAddr in enumerate(addresses.uniV1Exs):
        uniV1ExchangeAddressesToContracts[uniAddr] = W3.eth.contract(address=uniAddr, abi=ABIs.uniV1ExchangeABI)
        uniV1ExchangeAddressesToTokenAddresses[uniAddr] = addresses.uniV1Tokens[i]
        uniV1TokenAddressesToExchangeContracts[addresses.uniV1Tokens[i]] = uniV1ExchangeAddressesToContracts[uniAddr]
        addresses.uniV1ExsLower.append(uniAddr.lower())


    # Sets
    EX_NAME_TO_TOKEN_ADDRS_SET = {UNIV1: UNIV1_TOKEN_ADDRS_SET, KYBER: KYBER_TOKEN_ADDRS_SET, BANCOR: BANCOR_TOKEN_ADDRS_SET, UNIV2: UNIV2_TOKEN_ADDRS_SET}
    EX_NAME_TO_EX_ADDRS_SET = {UNIV1: set(addresses.uniV1Exs), KYBER: set([KYBER_PROXY_ADDRESS]), BANCOR: set([BANCOR_PROXY_ADDR]), UNIV2: set(addresses.uniV2Exs)}

    ALL_TOKEN_ADDRESSES_SET = set.union(*list(EX_NAME_TO_TOKEN_ADDRS_SET.values()))

    TOKEN_ADDRS_INTERSECTIONS = {}
    for srcSetName in EX_NAME_TO_TOKEN_ADDRS_SET:
        TOKEN_ADDRS_INTERSECTIONS[srcSetName] = {}
        for destSetName in EX_NAME_TO_TOKEN_ADDRS_SET:
            TOKEN_ADDRS_INTERSECTIONS[srcSetName][destSetName] = EX_NAME_TO_TOKEN_ADDRS_SET[srcSetName] & EX_NAME_TO_TOKEN_ADDRS_SET[destSetName]


    # Shared between all exchanges
    COMMON_TOKEN_ADDRS = set.intersection(*list(EX_NAME_TO_TOKEN_ADDRS_SET.values()))
    ALL_EXS_SET = set.union(*list(EX_NAME_TO_EX_ADDRS_SET.values()))
    EX_NAMES_TO_IDS = {UNIV1: 0, KYBER: 1, BANCOR: 2, UNIV2: 3}
    # TODO: update bancor in exIDToGasMiss
    EX_ID_TO_GAS_MISS = {EX_NAMES_TO_IDS[UNIV1]: 40000, EX_NAMES_TO_IDS[KYBER]: 110000, EX_NAMES_TO_IDS[BANCOR]: 150000, EX_NAMES_TO_IDS[UNIV2]: 30500}
    EX_NAME_TO_ETH_TOKEN_ADDR = {KYBER: W3.toChecksumAddress('0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'), BANCOR: W3.toChecksumAddress('0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'), UNIV2: WETH_ADDR}
    EX_ID_TO_ETH_TOKEN_ADDR = {EX_NAMES_TO_IDS[KYBER]: EX_NAME_TO_ETH_TOKEN_ADDR[KYBER], EX_NAMES_TO_IDS[BANCOR]: EX_NAME_TO_ETH_TOKEN_ADDR[BANCOR], EX_NAMES_TO_IDS[UNIV2]: EX_NAME_TO_ETH_TOKEN_ADDR[UNIV2]}
    TRADABLE_TOKENS = COMMON_TOKEN_ADDRS
    for srcSetName in EX_NAME_TO_TOKEN_ADDRS_SET:
        for destSetName in EX_NAME_TO_TOKEN_ADDRS_SET:
            if srcSetName != destSetName:
                TRADABLE_TOKENS = TRADABLE_TOKENS | TOKEN_ADDRS_INTERSECTIONS[srcSetName][destSetName]

    tokenAddressesToContracts = {}
    tokenAddressesToDecimals = {}
    for tokenAddr in ALL_TOKEN_ADDRESSES_SET:
        if tokenAddr not in EX_ID_TO_ETH_TOKEN_ADDR.values():
            tokenAddressesToContracts[tokenAddr] = W3.eth.contract(address=tokenAddr, abi=ABIs.tokenContractABI)
            if tokenAddr == '0xbdEB4b83251Fb146687fa19D1C660F99411eefe3':
                tokenAddressesToDecimals[tokenAddr] = 18
            elif tokenAddr == '0xE0B7927c4aF23765Cb51314A0E0521A9645F0E2A':
                tokenAddressesToDecimals[tokenAddr] = 9
            else:
                tokenAddressesToDecimals[tokenAddr] = tokenAddressesToContracts[tokenAddr].functions.decimals().call()


    TRADER_ADDRESS = W3.toChecksumAddress('REPLACE')
    TRADER_ADDRESS_LOWER = TRADER_ADDRESS.lower()
    OWNER_ADDR = 'REPLACE'
    TRADER = W3.eth.contract(address=TRADER_ADDRESS, abi=ABIs.traderABI)
    CONTRACT_INPUT_TO_TEST_TARGETS_PRICE = W3.toWei(0.1, 'ether')
    CONTRACT_TEST_OUTPUT_MULTIPLIER = 0.999999999
    GAS_LIMIT_FOR_GAS_ESTIMATE = 2000000
    GAS_LIMIT_ESTIMATE_MULTIPLIER = 1.1
    ETH_REVERT_REASON_HEX = '45746878'
    PENDING_TXS_FILTER = W3.eth.filter('pending')
    # addresses.pwners = addresses.pwners[::-1]
    # addresses.pwnerPrivKeys = addresses.pwnerPrivKeys[::-1]


    tokenAddressesNoAllowance = []
    for tokenAddr in ALL_TOKEN_ADDRESSES_SET:
        if tokenAddr not in EX_NAME_TO_ETH_TOKEN_ADDR.values() and tokenAddressesToContracts[tokenAddr].functions.allowance(TRADER_ADDRESS, BANCOR_PROXY.address).call() == 0:
            tokenAddressesNoAllowance.append(tokenAddr)

    # GST
    # NUM_TOKENS_UPPER_BOUND_GST1 = 40
    NUM_TOKENS_UPPER_BOUND_GST2 = 24
    # For 0.01 tokens
    # GAS_TO_MINT_GST1 = 20778
    GAS_TO_MINT_GST2 = 39883
    # ETH_TO_MINE_GST1 = 20778000000000
    ETH_TO_MINE_GST2 = GAS_TO_MINT_GST2 * (W3.toWei(1.5, 'gwei') + 1)
    GAS_TO_FREE_GST2 = 7000
    GAS_REFUND_GST2 = 24000
    NET_GAS_GAIN_GST2 = GAS_REFUND_GST2 - GAS_TO_FREE_GST2
    GST2_ADDRESS = '0x0000000000b3F879cb30FE243b4Dfee438691c04'
    GST2 = W3.eth.contract(address=GST2_ADDRESS, abi=ABIs.GSTABI)


    totalBal = getBalAllSelf()
    log('Total balance = {} = {}'.format(totalBal, totalBal / 10 ** 18), 'info')

    global getTxCntr
    global getTxAttemptsSingleRound
    global getTxFailedCntr
    global getTxTimeoutCntr
    global getTxFailSuccessCntr
    global getTxFailFailCntr
    getTxCntr, getTxFailedCntr, getTxTimeoutCntr, getTxFailSuccessCntr, getTxFailFailCntr = 0, 0, 0, 0, 0
    t2 = time.time()
    cntr = 0
    print('Searching')
    while True:
        cntr += 1
        searchMempool()
        time.sleep(0.01)
        if cntr % 200 == 0:
            print('cntr = ', cntr)
            if time.time() - t2 > 5:
                print('\ngetTxCntr = {}, getTxFailedCntr = {}, getTxTimeoutCntr = {}, getTxFailSuccessCntr = {}, getTxFailFailCntr = {}'.format(getTxCntr, getTxFailedCntr, getTxTimeoutCntr, getTxFailSuccessCntr, getTxFailFailCntr))
                print('Time for 200 rounds = ', time.time() - t2)
            getTxCntr, getTxFailedCntr, getTxTimeoutCntr = 0, 0, 0
            t2 = time.time()
        if cntr % 1000 == 0:
            if getTxFailSuccessCntr > 0 or getTxFailFailCntr > 0:
                print('\n blah getTxFailSuccessCntr = {}, getTxFailFailCntr = {}'.format(getTxFailSuccessCntr, getTxFailFailCntr))
            getTxFailSuccessCntr, getTxFailFailCntr = 0, 0
            updateTargetTxHashesToClones()
            writeToCloneResultsDB()
except Exception as e:
    # print('\ngetTxCntr = {}, getTxFailedCntr = {}, getTxTimeoutCntr = {}, getTxAttemptsSingleRound = {}'.format(getTxCntr, getTxFailedCntr, getTxTimeoutCntr, getTxAttemptsSingleRound))
    log('Exception occurred: {}'.format(e), 'error')
    # log('fullTrace = {}'.format(fullTrace), 'info')
    log(traceback.format_exc(), 'error')

