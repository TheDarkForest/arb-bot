import time
import datetime
from web3 import Web3, IPCProvider, WebsocketProvider, HTTPProvider, exceptions
import logging
import addresses
import uniV1ExchangeContractABI
import tokenContractABI
import math
import traceback
import kyberProxyABI
import traderABI
from scipy.optimize import minimize_scalar
import GSTABI



def now():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def log(text, level):
    text = '{}\n'.format(text)
    print(text)
    if level == 'debug':
        logger.debug(text)
    if level == 'info':
        logger.info(text)
    if level == 'warning':
        logger.warning(text)
    if level == 'error':
        logger.error(text)
    if level == 'critical':
        logger.critical(text)


def signSendTx(txDict, name, privKey):
    signedTx = W3.eth.account.signTransaction(txDict, private_key=privKey)
    log('{} = {}'.format(name, txDict), 'info')

    try:
        result = W3.eth.sendRawTransaction(signedTx.rawTransaction)
        log('result {} = {}'.format(name, W3.toHex(result)), 'critical')
    except Exception as e:
        log('ERROR: {}'.format(e), 'error')



try:
    t0 = time.time()
    VERBOSE_LOG_FILE_NAME = 'miner.log'

    logging.basicConfig(
        filename=VERBOSE_LOG_FILE_NAME,
        filemode='a',
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s     %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.INFO
    )
    logger = logging.getLogger('arber')
    log('-----------STARTING-----------', 'info')

    IPC_PATH = '/root/.local/share/openethereum/jsonrpc.ipc'
    W3 = Web3(IPCProvider(IPC_PATH))
    # TODO: WARNING, HAS BEEN USED FROM OLD PARITY MACHINE
    INFURA = Web3(WebsocketProvider('REPLACE'))

    FILTER = W3.eth.filter('pending')
    productionMode = True
    if not productionMode: log('WARNING! CURRENTLY IN TESTING MODE!', 'info')

    TRICKSTER_PRIVATE_KEY = 'REPLACE'
    TRICKSTER_ADDRESS = W3.toChecksumAddress('REPLACE')
    pwnerAddressesToPrivKeys = {}
    pwnerPrivKeysToAddresses = {}
    pwnerAddressesToPendingTxs = {}
    for i, addr in enumerate(addresses.pwners):
        pwnerAddressesToPrivKeys[addr] = addresses.pwnerPrivKeys[i]
        pwnerPrivKeysToAddresses[addresses.pwnerPrivKeys[i]] = addr
        pwnerAddressesToPendingTxs[addr] = {}


    # ETH_TO_KEEP_IN_WALLET = W3.toWei(0.1, 'ether')
    ETH_TO_KEEP_IN_WALLET = 0
    MIN_GAS_PRICE = W3.toWei(1, 'gwei')
    # if productionMode:
    #     MIN_ETH = W3.toWei(0.1, 'ether')
    # else:
    #     MIN_ETH = 0
    global timeToCleanUp
    timeToCleanUp = time.time()
    global gasPriceOfMostRecentTx
    gasPriceOfMostRecentTx = 5*10**9
    global dictTargetFromAddrToDictTxsToRuns
    dictTargetFromAddrToDictTxsToRuns = {}
    ETH_TO_TEST_PRICE = W3.toWei(0.1, 'ether')
    TRACE_TIME_TO_FLAG = 0.5

    UNISWAP_TOKEN_ADDRESSES_SET = set(addresses.uniV1Tokens)
    KYBER_TOKEN_ADDRESSES_SET = set(addresses.kyberTokens)

    ALL_TOKEN_ADDRESSES_SET = UNISWAP_TOKEN_ADDRESSES_SET | KYBER_TOKEN_ADDRESSES_SET
    TOKEN_ADDRESSES_SETS = {'uniswap': UNISWAP_TOKEN_ADDRESSES_SET, 'kyber': KYBER_TOKEN_ADDRESSES_SET}
    TOKEN_ADDRESSES_INTERSECTIONS = {}
    for srcSetName in TOKEN_ADDRESSES_SETS:
        intersections = {}
        for destSetName in TOKEN_ADDRESSES_SETS:
            if srcSetName != destSetName:
                intersections[destSetName] = TOKEN_ADDRESSES_SETS[srcSetName] & TOKEN_ADDRESSES_SETS[destSetName]

        TOKEN_ADDRESSES_INTERSECTIONS[srcSetName] = intersections


    # Kyber
    KYBER_PROXY_ADDRESS = '0x818E6FECD516Ecc3849DAf6845e3EC868087B755'
    KYBER_PROXY_ADDRESS_LOWER = KYBER_PROXY_ADDRESS.lower()
    KYBER_PROXY = W3.eth.contract(address=KYBER_PROXY_ADDRESS, abi=kyberProxyABI.kyberProxyABI)
    ETH_TOKEN_ADDRESS_LOWER = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    ETH_TOKEN_ADDRESS = W3.toChecksumAddress(ETH_TOKEN_ADDRESS_LOWER)

    SUPPORTED_KYBER_FUNCTIONS_FULL = [
        '<Function tradeWithHint(address,uint256,address,address,uint256,uint256,address,bytes)>',
    ]

    # Shared between all exchanges
    tokenAddressesToContracts = {}
    tokenAddressesToDecimals = {}
    for tokenAddr in ALL_TOKEN_ADDRESSES_SET:
        if tokenAddr != ETH_TOKEN_ADDRESS:
            tokenAddressesToContracts[tokenAddr] = W3.eth.contract(address=tokenAddr, abi=tokenContractABI.tokenContractABI)
            tokenAddressesToDecimals[tokenAddr] = tokenAddressesToContracts[tokenAddr].functions.decimals().call()

    COMMON_TOKEN_ADDRESSES = UNISWAP_TOKEN_ADDRESSES_SET & KYBER_TOKEN_ADDRESSES_SET
    ALL_EXCHANGES_SET = set(addresses.uniV1Exs + [KYBER_PROXY_ADDRESS])
    exchangeNamesToIDs = {'uniswap': 0, 'kyber': 1}

    # Uniswap
    SUPPORTED_UNISWAP_FUNCTIONS_FULL = [
        '<Function ethToTokenSwapInput(uint256,uint256)>',
        '<Function ethToTokenTransferInput(uint256,uint256,address)>',
        '<Function ethToTokenSwapOutput(uint256,uint256)>',
        '<Function ethToTokenTransferOutput(uint256,uint256,address)>',

        '<Function tokenToEthSwapInput(uint256,uint256,uint256)>',
        '<Function tokenToEthTransferInput(uint256,uint256,uint256,address)>',
        '<Function tokenToEthSwapOutput(uint256,uint256,uint256)>',
        '<Function tokenToEthTransferOutput(uint256,uint256,uint256,address)>',

        '<Function tokenToTokenSwapInput(uint256,uint256,uint256,uint256,address)>',
        '<Function tokenToTokenTransferInput(uint256,uint256,uint256,uint256,address,address)>',
        '<Function tokenToTokenSwapOutput(uint256,uint256,uint256,uint256,address)>',
        '<Function tokenToTokenTransferOutput(uint256,uint256,uint256,uint256,address,address)>',
    ]
    uniExchangeAddressesToContracts = {}
    uniExchangeAddressesToTokenAddresses = {}
    uniExchangeAddressesToTokenContracts = {}
    uniTokenAddressesToExchangeContracts = {}
    for i, uniAddr in enumerate(addresses.uniV1Exs):
        uniExchangeAddressesToContracts[uniAddr] = W3.eth.contract(address=uniAddr, abi=uniV1ExchangeContractABI.uniV1ExchangeABI)
        uniExchangeAddressesToTokenAddresses[uniAddr] = addresses.uniV1Tokens[i]
        uniExchangeAddressesToTokenContracts[uniAddr] = tokenAddressesToContracts[addresses.uniV1Tokens[i]]
        uniTokenAddressesToExchangeContracts[addresses.uniV1Tokens[i]] = uniExchangeAddressesToContracts[uniAddr]



    TRADER_ADDRESS = W3.toChecksumAddress('REPLACE')
    TRADER = W3.eth.contract(address=TRADER_ADDRESS, abi=traderABI.traderABI)
    CONTRACT_INPUT_TO_TEST_TARGETS_PRICE = W3.toWei(0.1, 'ether')
    CONTRACT_TEST_OUTPUT_MULTIPLIER = 0.999999999
    GAS_LIMIT_FOR_GAS_ESTIMATE = 2000000
    GAS_LIMIT_ESTIMATE_MULTIPLIER = 1.25
    GAS_LIMIT_FAILED_TX = 65000
    SUCCESS_PROB_OF_CLONE_TRADES = 0.25

    # GST
    NUM_TOKENS_UPPER_BOUND_GST1 = 401
    NUM_TOKENS_UPPER_BOUND_GST2 = 243
    # MIN_NUM_TOKENS_FACTOR = 50
    MIN_NUM_TOKENS_FACTOR = 10
    # MAX_NUM_TOKENS_FACTOR = 100
    MAX_NUM_TOKENS_FACTOR = 20
    GST1Address = W3.toChecksumAddress('0x88d60255F917e3eb94eaE199d827DAd837fac4cB')
    GST1 = W3.eth.contract(address=GST1Address, abi=GSTABI.GSTABI)
    GST2Address = W3.toChecksumAddress('0x0000000000b3F879cb30FE243b4Dfee438691c04')
    GST2 = W3.eth.contract(address=GST2Address, abi=GSTABI.GSTABI)
    GST_GAS_PRICE = W3.toWei(10.11, 'gwei') + 1
    GST_GAS_LIMIT = 600000
    NUM_GST1_TOKENS_PER_TX = 47
    NUM_GST2_TOKENS_PER_TX = 10



    def mineBatch(GST, numTokensPerTx, upperBoundSingleTx, nonce):
        gstBal = GST.functions.balanceOf(TRADER_ADDRESS).call()
        minNum = upperBoundSingleTx * MIN_NUM_TOKENS_FACTOR
        if gstBal >= minNum:
            log('Bal = {} is above the min = {}'.format(gstBal, minNum))
            return

        maxNum = upperBoundSingleTx * MAX_NUM_TOKENS_FACTOR
        numToMine = maxNum - gstBal
        numTotalTxs = int(numToMine / numTokensPerTx)

        for i in range(numTotalTxs):
            mine(GST, numTokensPerTx, nonce, i)
            nonce += 1
            time.sleep(0.2)

        return nonce

    def mine(GST, numTokensPerTx, nonce, i):
        chainParams = {
            'chainId': 1,
            'gasPrice': GST_GAS_PRICE,
            'gas': GST_GAS_LIMIT,
            'nonce': nonce
        }
        miningTx = GST.functions.mint(numTokensPerTx).buildTransaction(chainParams)
        signSendTx(miningTx, 'miningTx{}'.format(i), TRICKSTER_PRIVATE_KEY)
        print(W3.eth.estimateGas(miningTx))


    def sendToSelf(nonce, i):
        chainParams = {
            'chainId': 1,
            'gasPrice': W3.toWei(1, 'gwei'),
            'gas': 21000,
            'nonce': nonce
        }
        chainParams['to'] = TRICKSTER_ADDRESS
        chainParams['value'] = 0
        selfTx = chainParams
        signSendTx(selfTx, 'selfTx{}'.format(i), TRICKSTER_PRIVATE_KEY)

    nonce = W3.eth.getTransactionCount(TRICKSTER_ADDRESS)
    # for i in range(240):
    #     sendToSelf(nonce, i)
    #     nonce += 1
    #     time.sleep(1)
    # sendToSelf(nonce, 0)

    # Test
    # mine(GST2, NUM_GST2_TOKENS_PER_TX, nonce, 0)
    # nonce += 1
    # mine(GST1, NUM_GST1_TOKENS_PER_TX, nonce, 0)

    nonce = mineBatch(GST2, NUM_GST2_TOKENS_PER_TX, NUM_TOKENS_UPPER_BOUND_GST2, nonce)
    # mineBatch(GST1, NUM_TOKENS_UPPER_BOUND_GST1, nonce)

except Exception as e:
    log('Exception occurred: {}'.format(e), 'error')
    log(traceback.format_exc(), 'error')
