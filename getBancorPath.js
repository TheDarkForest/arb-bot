const BancorSDK = require('@bancor/sdk').SDK;
const Net = require('net');
const Web3 = require('web3');


async function getPath(srcTokenAddr, destTokenAddr) {

    try {
        const settings = {
            ethereumNodeEndpoint: new Web3.providers.IpcProvider('/root/.local/share/openethereum/jsonrpc.ipc', Net),
        };
//        console.log('a');
        let bancorSDK = await BancorSDK.create(settings);
//        console.log('b');
        const sourceToken = {
            blockchainType: 'ethereum',
            blockchainId: srcTokenAddr
        };
//        console.log('c');
        const targetToken = {
            blockchainType: 'ethereum',
            blockchainId: destTokenAddr
        };
//        console.log('d');
        const res = await bancorSDK.pricing.getPathAndRate(sourceToken, targetToken, "1.0");
//        console.log('e');
        res.path.forEach(item => console.log(item.blockchainId));
//        console.log('f');
        await BancorSDK.destroy(bancorSDK);
    }
    catch (err) {
        console.log('err');
//        console.log(err);
    }
    finally {
        process.exit();
    }
}

const args = process.argv;
getPath(args[2], args[3]);